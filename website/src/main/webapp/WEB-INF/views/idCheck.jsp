<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>박박박꽃배달</title>

<style type="text/css">
.btn {
	width: 100px;
	height: 30px;
	margin: 10px;
	background-color: rgb(82, 82, 82);
	color: white;
	border: 0.1px solid rgba(0, 0, 0, 0.329);
	font-size: 13px;
	float: right;
}

.sch {
	width: 200px;
	height: 20px;
	margin: 10px;
}

.box {
	/* 	padding: 10px; */
	
}

.result {
	display: block;
	width: 300px;
	float: left;
	margin-top: 13px;
}

* {
	font-size: 16px;
	font-weight: bold;
}
</style>

</head>

<body>

	<form action="idCheck" method="get" name="userInfo">
		<div class="box">
			아이디 <input type="text" name="userId" value="${userId }" class="sch"> 
			<input type="submit" value="중복체크" class="btn"><br>
			<hr>

			<c:choose>
				<c:when test="${result == 1}">
					<script type="text/javascript">
						opener.document.userInfo.userId.value = "";
					</script>
					<div class="result">
						<font color=red>[${userId }]</font>는 이미 사용중인 아이디 입니다.
					</div>
				</c:when>
				<c:when test="${userId == ''}">
					<script type="text/javascript">
						opener.document.userInfo.userId.value = "";
					</script>
					<div class="result">아이디를 확인해주세요.</div>
				</c:when>
				<c:when test="${fn:length(userId) < 4 || fn:length(userId) > 15}">
					<script type="text/javascript">
						opener.document.userInfo.userId.value = "";
					</script>
					<div class="result">아이디는 4~15자 이내여야 합니다.</div>
				</c:when>
				<c:when test="${userId.indexOf(' ') >= 0}">
					<script type="text/javascript">
						opener.document.userInfo.userId.value = "";
					</script>
					<div class="result">아이디에 공백이 들어가면 안됩니다.</div>
				</c:when>
				<c:otherwise>
					<div class="result">
						<font color=red>[${userId }]</font>는 사용 가능한 아이디 입니다.
					</div>
					<input type="button" value="사용" onclick="idok('${userId}')" class="btn">
				</c:otherwise>

			</c:choose>
		</div>
	</form>

	<script>
		function idok(userId) {
			opener.userInfo.userId.value = userId;
			opener.userInfo.reId.value = userId;
			window.close();
		};
	</script>

</body>

</html>