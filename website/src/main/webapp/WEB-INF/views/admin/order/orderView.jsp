<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<link rel="stylesheet" href="/resources/css/admin/order/orderView.css" />


<html>
<head>
<title>Board Admin</title>

<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

</head>
<body>

	<div id="root">
		<header id="header">
			<div id="header_box">
				<%@ include file="../include/header.jsp"%>
			</div>
		</header>
		<nav id="nav">
			<div id="nav_box"><%@ include file="../include/nav.jsp"%></div>
		</nav>

		<section id="container">
			<div id="aside">
				<%@ include file="../include/aside.jsp"%>
			</div>

			<div>
				<table class="orderViewfrom">
					<colgroup>
						<col width="200px">
						<col width="300px">
						<col width="300px">
						<col width="300px">
						<col width="300px">
						<col width="300px">
						<col width="300px">
					</colgroup>
					<tbody>
						<tr class="orderViewName">
							<td>주문번호</td>
							<td>상품명</td>
							<td>이미지</td>
							<td>결제금액</td>
							<td>결제일자</td>
							<td>배송상태</td>
							<td>배송상태수정</td>
						</tr>
						<c:forEach var="list" items="${list }">
							<tr class="orderViewInfo">
								<td>${list.orderNum }</td>
								<td>${list.productName }</td>
								<td><img src="/resources/images/${list.pictureUrl}"></td>
								<td><fmt:formatNumber value="${list.sumMoney }" pattern="#,###,###" /> 원</td>
								<td><fmt:formatDate value="${list.orderDate }" pattern="yyyy-MM-dd HH:mm:ss" /></td>
								<td>${list.delivery }</td>
								<td><div class="deliveryChange">
										<form role="form" method="post" class="deliveryForm">
											
											<input type="hidden" name="delivery" class="delivery" value="" />

											<button type="button" class="delivery1_btn">배송 중</button>
											<button type="button" class="delivery2_btn">배송 완료</button>
											<script>
												$(function() {
													$(".delivery1_btn").click(function() {
														$(".delivery").val("배송 중");
														run();
													});
													$(".delivery2_btn").click(function() {
														$(".delivery").val("배송 완료");
														run();
													});
													function run() {
														$(".deliveryForm").submit();
													}
												});
											</script>
										</form>
									</div></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>

				<p class="title1">주문자 정보</p>

				<form role="form" method="post" autocomplete="off">

					<table class="orderUserInfo">
						<colgroup>
							<col width="200px">
							<col width="900px">
						</colgroup>
						<tbody>
							<c:forEach var="list" items="${list }">
								<tr>
									<td>이름</td>
									<td>${list.orderUser }</td>
								</tr>
								<tr>
									<td>핸드폰번호</td>
									<td>${list.orderPhone }</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>


					<p class="title2">배송지 정보</p>

					<table class="orderAgreePost">
						<colgroup>
							<col width="200px">
							<col width="900px">
						</colgroup>
						<tbody>
							<c:forEach var="list" items="${list }">
								<tr>
									<td class="orderAgreeTitle">수령인</td>
									<td colspan="3" class="orderAgreeContent">${list.orderRec }</td>
								</tr>
								<tr>
									<td class="orderAgreeTitle">연락처</td>
									<td colspan="3" class="orderAgreeContent">${list.orderRecPhone}</td>
								</tr>
								<tr>
									<td class="orderAgreeTitle">주소지</td>
									<td class="orderAgreeContent">${list.orderAddr1}<br></td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td class="orderAgreeContent">${list.orderAddr2}<br></td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td class="orderAgreeContent">${list.orderAddr3}<br></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</form>
			</div>
		</section>

		<footer id="footer">
			<div id="footer_box"><%@ include file="../include/footer.jsp"%></div>
		</footer>
	</div>
</body>
</html>