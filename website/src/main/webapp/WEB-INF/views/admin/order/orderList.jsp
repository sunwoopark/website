<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<link rel="stylesheet" href="/resources/css/admin/order/orderList.css" />


<html>
<head>
<title>Board Admin</title>

<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

</head>
<body>

	<div id="root">
		<header id="header">
			<div id="header_box">
				<%@ include file="../include/header.jsp"%>
			</div>
		</header>
		<nav id="nav">
			<div id="nav_box"><%@ include file="../include/nav.jsp"%></div>
		</nav>

		<section id="container">
			<div id="aside">
				<%@ include file="../include/aside.jsp"%>
			</div>

			<div id="container_box">
				<!-- <h2>상품 목록</h2> -->
				<form name="form1">
					<table class="orderList">
						<colgroup>
							<col width="300px">
							<col width="300px">
							<col width="300px">
							<col width="300px">
							<col width="300px">
							<col width="300px">
						</colgroup>
						<thead>
							<tr class="orderListTitle">
								<td>주문번호</td>
								<td>상품명</td>
								<td>이미지</td>
								<td>결제일자</td>
								<td>결제금액</td>
								<td>배송상태</td>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${list}" var="list">
								<tr class="orderListInfo">
									<td><a href="/admin/orderView?orderNum=${list.orderNum }">${list.orderNum }</a></td>
									<td>${list.productName }</td>
									<td><img src="/resources/images/${list.pictureUrl}"></td>
									<td><fmt:formatDate value="${list.orderDate }" pattern="yyyy-MM-dd HH:mm:ss" /></td>
									<td><fmt:formatNumber value="${list.sumMoney }" pattern="#,###,###" /> 원</td>
									<td>${list.delivery }</td>
								</tr>
							</c:forEach>

						</tbody>
					</table>
				</form>
			</div>
		</section>

		<footer id="footer">
			<div id="footer_box"><%@ include file="../include/footer.jsp"%></div>
		</footer>
	</div>
</body>
</html>