<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<link rel="stylesheet" href="/resources/css/admin/include/aside.css" />


<ul>
	<li><a href="/admin/productRegister">상품 등록</a></li>
	<li><a href="/admin/productList">상품 목록</a></li>
	<li><a href="/admin/orderList">주문 목록</a></li>
	<li><a href="">상품 소감</a></li>
	<li><a href="/admin/userList">유저 목록</a></li>
	<li><a href="/admin/board/boardList">게시글 관리</a></li>
</ul>
