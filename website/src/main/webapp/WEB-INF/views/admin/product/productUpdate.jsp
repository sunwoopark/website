<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<link rel="stylesheet" href="/resources/css/admin/productDetail.css" />


<html>
<head>
<title>Board Admin</title>


<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="/resources/js/include/go-back.js"></script>
<script src="/resources/ckeditor/ckeditor.js"></script>
</head>
<body>

	<div id="root">
		<header id="header">
			<div id="header_box">
				<%@ include file="../include/header.jsp"%>
			</div>
		</header>
		<nav id="nav">
			<div id="nav_box"><%@ include file="../include/nav.jsp"%></div>
		</nav>

		<section id="container">
			<div id="aside">
				<%@ include file="../include/aside.jsp"%>
			</div>
			<div id="container_box">
				<form method="post" autocomplete="off" action="/admin/productUpdateResult" enctype="multipart/form-data">
					<input type="hidden" value="${dto.productId}" name="productId">
					<div class="flex-row-start">
						<div class="pictureUrl">
							<img src="/resources/images/${dto.pictureUrl}" width="200px" height="200px">
						</div>
						<div class="product-info">
							<table>
								<colgroup>
									<col width="100px">
									<col width="200px">
									<col width="100px">
									<col width="200px">
								</colgroup>
								<tbody>
									<tr>
										<td>코드번호</td>
										<td colspan="3">${dto.productId}[수정불가]</td>
									</tr>
									<tr>
										<td>카테고리</td>
										<td><select name="productCate1" style="width: 200px;">
												<option value="" selected="">-----</option>
												<option value="1" <c:if test="${dto.productCate1 == 1 }">selected</c:if>>꽃다발</option>
												<option value="2" <c:if test="${dto.productCate1 == 2 }">selected</c:if>>꽃바구니</option>
												<option value="3" <c:if test="${dto.productCate1 == 3 }">selected</c:if>>꽃상자</option>
												<option value="4" <c:if test="${dto.productCate1 == 4 }">selected</c:if>>축하화환</option>
												<option value="5" <c:if test="${dto.productCate1 == 5 }">selected</c:if>>근조화환</option>
												<option value="6" <c:if test="${dto.productCate1 == 6 }">selected</c:if>>동양란</option>
												<option value="7" <c:if test="${dto.productCate1 == 7 }">selected</c:if>>서양란</option>
												<option value="8" <c:if test="${dto.productCate1 == 8 }">selected</c:if>>공기정화식물</option>
												<option value="9" <c:if test="${dto.productCate1 == 9 }">selected</c:if>>기타</option>
										</select></td>
									<tr>
										<td>카테고리2</td>
										<td><select name="productCate2" style="width: 200px;">
												<option value="1" <c:if test="${dto.productCate2 == 1 }">selected</c:if>>기본(없음)</option>
												<option value="2" <c:if test="${dto.productCate2 == 2 }">selected</c:if>>베스트상품</option>
												<option value="3" <c:if test="${dto.productCate2 == 3 }">selected</c:if>>카테고리상품</option>
												<option value="4" <c:if test="${dto.productCate2 == 4 }">selected</c:if>>초이스상품</option>
												<option value="5" <c:if test="${dto.productCate2 == 5 }">selected</c:if>>기타</option>
										</select></td>
									</tr>
									<tr>
										<td>상품명</td>
										<td colspan="3"><input type="text" value="${dto.productName }" name="productName"></td>
									</tr>
									<tr>
										<td>상품가격</td>
										<td colspan="3"><input type="text" value="${dto.price}" name="price"></td>
									</tr>
									<tr>
										<td>이미지</td>
										<td colspan="3"><input type="file" name="file1"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<br>
					<hr>
					<br>
					<p>상세 내용</p>
					<div class="product-info-detail flex-row-start">
						<div class="detail-img">
							<img src="/resources/images/${dto.pictureUrl}" />
						</div>
						<div>
							<textarea rows="5" cols="50" id="description" name="description">${dto.description }</textarea>
							<script>
								CKEDITOR.replace("description", {
									filebrowserUploadUrl : "/imageUpload"
								});
							</script>
						</div>
					</div>

					<div class="btn-class" style="text-align: center;">
						<input type="submit" value="저 장" class="update-btn"> <input type="button" value="취 소" class="go-back">
					</div>
				</form>
			</div>
		</section>

		<footer id="footer">
			<div id="footer_box"><%@ include file="../include/footer.jsp"%></div>
		</footer>
	</div>
</body>
</html>

