<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<link rel="stylesheet" href="/resources/css/admin/productList.css" />


<html>
<head>
<title>Board Admin</title>

<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

</head>
<body>

	<div id="root">
		<header id="header">
			<div id="header_box">
				<%@ include file="../include/header.jsp"%>
			</div>
		</header>
		<nav id="nav">
			<div id="nav_box"><%@ include file="../include/nav.jsp"%></div>
		</nav>

		<section id="container">
			<div id="aside">
				<%@ include file="../include/aside.jsp"%>
			</div>

			<div id="container_box">
				<!-- <h2>상품 목록</h2> -->
				<form name="form1">
					<table class="goodsList">
						<colgroup>
							<col width="100px">
							<col width="400px">
							<col width="400px">
							<col width="400px">
							<col width="400px">
							<col width="400px">
							<col width="200px">
						</colgroup>
						<thead>
							<tr>
								<th>코드번호</th>
								<th>이름</th>
								<th>카테고리1</th>
								<th>카테고리2</th>
								<th>가격</th>
								<th>이미지</th>
								<th>수정 / 삭제</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${list}" var="list">
								<tr>
									<td>${list.productId }</td>
									<td><a href="/admin/productDetail?n=${list.productId }">${list.productName } / [상세보기]</a></td>
									<td><c:if test="${(list.productCate1 == 1)}">꽃다발</c:if> <c:if test="${(list.productCate1 == 2)}">꽃바구니</c:if> <c:if
											test="${(list.productCate1 == 3)}">꽃상자</c:if> <c:if test="${(list.productCate1 == 4)}">축하화환</c:if> <c:if test="${(list.productCate1 == 5)}">근조화환</c:if>
										<c:if test="${(list.productCate1 == 6)}">동양란</c:if> <c:if test="${(list.productCate1 == 7)}">서양란</c:if> <c:if
											test="${(list.productCate1 == 8)}">공기정화식물</c:if> <c:if test="${(list.productCate1 == 9)}">기타</c:if></td>
									<td><c:if test="${(list.productCate2 == 1)}">기본</c:if> <c:if test="${(list.productCate2 == 2)}">베스트상품</c:if> <c:if
											test="${(list.productCate2 == 3)}">카테고리상품</c:if> <c:if test="${(list.productCate2 == 4)}">초이스상품</c:if> <c:if
											test="${(list.productCate2 == 5)}">기타</c:if></td>
									<td><fmt:formatNumber value="${list.price}" pattern="###,###,###" /></td>
									<td><img src="/resources/images/${list.pictureUrl}" width="100px" height="100px"></td>
									<td><a href="/admin/productUpdate?productId=${list.productId}">수정</a> / 
										<a href="/admin/productDelete?productId=${list.productId}" onclick="return confirm('삭제하시겠습니까?')">삭제</a>
									</td>
								</tr>
							</c:forEach>

						</tbody>
					</table>
				</form>
			</div>
		</section>

		<footer id="footer">
			<div id="footer_box"><%@ include file="../include/footer.jsp"%></div>
		</footer>
	</div>
</body>
</html>