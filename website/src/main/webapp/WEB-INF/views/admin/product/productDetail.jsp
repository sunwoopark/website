<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<link rel="stylesheet" href="/resources/css/admin/productDetail.css" />


<html>
<head>
<title>Board Admin</title>

<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="/resources/js/include/go-back.js"></script>
<script>
function product_delete() {
	if (confirm("삭제하시겠습니까?")) {
		document.form1.action = "/admin/productDelete";
		document.form1.submit();
	};
};
</script>

</head>
<body>

	<div id="root">
		<header id="header">
			<div id="header_box">
				<%@ include file="../include/header.jsp"%>
			</div>
		</header>
		<nav id="nav">
			<div id="nav_box"><%@ include file="../include/nav.jsp"%></div>
		</nav>

		<section id="container">
			<div id="aside">
				<%@ include file="../include/aside.jsp"%>
			</div>
			<div id="container_box">
				<form name="form1" method="post" action="/admin/productUpdate"> 
					<input type="hidden" value="${dto.productId}" name="productId">
					<div class="flex-row-start">
						<div class="pictureUrl">
							<img src="/resources/images/${dto.pictureUrl}" width="200px" height="200px">
						</div>
						<div class="product-info">
							<table>
								<colgroup>
									<col width="100px">
									<col width="200px">
									<col width="100px">
									<col width="200px">
								</colgroup>
								<tbody>
									<tr>
										<td>코드번호</td>
										<td colspan="3">${dto.productId}</td>
									</tr>
									<tr>
										<td>카테고리</td>
										<td><c:if test="${(dto.productCate1 == 1)}">꽃다발</c:if> <c:if test="${(dto.productCate1 == 2)}">꽃바구니</c:if> <c:if
												test="${(dto.productCate1 == 3)}">꽃상자</c:if> <c:if test="${(dto.productCate1 == 4)}">축하화환</c:if> <c:if test="${(dto.productCate1 == 5)}">근조화환</c:if>
											<c:if test="${(dto.productCate1 == 6)}">동양란</c:if> <c:if test="${(dto.productCate1 == 7)}">서양란</c:if> <c:if test="${(dto.productCate1 == 8)}">공기정화식물</c:if>
											<c:if test="${(dto.productCate1 == 9)}">기타</c:if></td>
									<tr>
										<td>상품명</td>
										<td colspan="3">${dto.productName }</td>
									</tr>
									<tr>
										<td>상품가격</td>
										<td colspan="3"><fmt:formatNumber value="${dto.price}" pattern="###,###,###" /></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<br>
					<hr>
					<br>
					<p>상세 내용</p>
					<div class="product-info-detail flex-row-start">
						<div class="detail-img">
							<img src="/resources/images/${dto.pictureUrl}" />
						</div>
						<div>
							<div>${dto.description }</div>
						</div>
					</div>

					<div class="btn-class" style="text-align: center;">
						<input type="submit" value="수 정" class="update-btn">
						<input type="button" value="취 소" class="go-back">
						<input type="button" value="삭 제" class="delete-btn" onclick="product_delete()">
					</div>
				</form>
			</div>
		</section>

		<footer id="footer">
			<div id="footer_box"><%@ include file="../include/footer.jsp"%></div>
		</footer>
	</div>
</body>
</html>

