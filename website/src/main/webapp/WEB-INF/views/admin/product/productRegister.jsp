<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<link rel="stylesheet" href="/resources/css/admin/productRegister.css" />


<html>
<head>
<title>Board Admin</title>

<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="/resources/ckeditor/ckeditor.js"></script>
<script src="/resources/js/include/go-back.js"></script>

</head>
<body>

	<div id="root">
		<header id="header">
			<div id="header_box">
				<%@ include file="../include/header.jsp"%>
			</div>
		</header>
		<nav id="nav">
			<div id="nav_box"><%@ include file="../include/nav.jsp"%></div>
		</nav>

		<section id="container">
			<div id="aside">
				<%@ include file="../include/aside.jsp"%>
			</div>

			<div id="container_box">
				<form action="productRegisterResult" id="form1" name="form1" method="post" enctype="multipart/form-data">

					<table>
						<colgroup>
							<col width="100px">
							<col width="150px">
							<col width="100px">
							<col width="150px">
						</colgroup>
						<tbody>
							<tr>
								<td>카테고리1</td>
								<td><select name="productCate1" style="width: 200px;">
										<option value="1" selected>꽃다발</option>
										<option value="2">꽃바구니</option>
										<option value="3">꽃상자</option>
										<option value="4">축하화환</option>
										<option value="5">근조화환</option>
										<option value="6">동양란</option>
										<option value="7">서양란</option>
										<option value="8">공기정화식물</option>
										<option value="9">기타</option>
								</select></td>
							</tr>
							<tr>
								<td>카테고리2</td>
								<td><select name="productCate2" style="width: 200px;">
										<option value="1" selected>기본(없음)</option>
										<option value="2">베스트상품</option>
										<option value="3">카테고리상품</option>
										<option value="4">초이스상품</option>
										<option value="5">기타</option>
								</select></td>
							</tr>
							<tr>
								<td><label>상품명</label></td>
								<td colspan="3"><input type="text" id="productName" name="productName" /></td>
							</tr>
							<tr>
								<td><label>상품가격</label></td>
								<td colspan="3"><input type="text" id="price" name="price"></td>
							</tr>
							<tr>
								<td>상품이미지</td>
								<td><input type="file" name="file1"></td>
							</tr>
						</tbody>
					</table>

					<div>
						<textarea rows="5" cols="50" id="description" name="description"></textarea>
					</div>
					<script>
						CKEDITOR.replace("description", {
							filebrowserUploadUrl : "/imageUpload"
						});
					</script>

					<div>
						저장위치 :
						<%=request.getRealPath("/")%>
					</div>

					<div style="text-align: center;">
						<input type="submit" value="등 록" class="register-btn"> <input type="button" value="목 록" class="go-back">
					</div>

				</form>
			</div>
		</section>

		<footer id="footer">
			<div id="footer_box"><%@ include file="../include/footer.jsp"%></div>
		</footer>
	</div>
</body>
</html>

