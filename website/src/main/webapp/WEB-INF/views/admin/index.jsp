<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<link rel="stylesheet" href="/resources/css/admin/index.css" />
<html>
<head>
<title>Board Admin</title>

</head>
<body>

	<div id="root">
		<header id="header">
			<div id="header_box">
				<%@ include file="include/header.jsp"%>
			</div>
		</header>
		<nav id="nav">
			<div id="nav_box"><%@ include file="include/nav.jsp"%></div>
		</nav>

		<section id="container">
			<div id="aside">
				<%@ include file="include/aside.jsp"%>
			</div>
			<div id="container_box">관리자 화면</div>
		</section>


		<footer id="footer">
			<div id="footer_box"><%@ include file="include/footer.jsp"%></div>
		</footer>
	</div>
</body>
</html>