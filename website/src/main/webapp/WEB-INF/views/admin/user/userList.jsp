<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<link rel="stylesheet" href="/resources/css/admin/userList.css" />


<html>
<head>
<title>Board Admin</title>

<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

</head>
<body>

	<div id="root">
		<header id="header">
			<div id="header_box">
				<%@ include file="../include/header.jsp"%>
			</div>
		</header>
		<nav id="nav">
			<div id="nav_box"><%@ include file="../include/nav.jsp"%></div>
		</nav>

		<section id="container">
			<div id="aside">
				<%@ include file="../include/aside.jsp"%>
			</div>
			<div id="container_box">
				<div class="memberInfo">
					<table>
						<colgroup>
							<col width="150px">
							<col width="150px">
							<col width="150px">
							<col width="150px">
							<col width="250px">
							<col width="250px">
							<col width="250px">
							<col width="250px">
							<col width="250px">
						</colgroup>
						<thead class="memberInfoTitle">
							<tr>
								<th>userNum</th>
								<th>userId</th>
								<th>userName</th>
								<th>userGender</th>
								<th>userBirth</th>
								<th>userPhone</th>
								<th>userEmail</th>
								<th>joinDate</th>
								<th>/</th>
							</tr>
						</thead>
						<tbody class="memberInfoContent">
							<c:forEach items="${userInfo}" var="userInfo">
								<tr>
									<td>${userInfo.userNum}</td>
									<td>${userInfo.userId}</td>
									<td>${userInfo.userName}</td>
									<td>${userInfo.userGender}</td>
									<td>${fn:substring(userInfo.userBirth,0,4)}- ${fn:substring(userInfo.userBirth,4,6)} - ${fn:substring(userInfo.userBirth,6,8)}</td>
									<td>${fn:substring(userInfo.userPhone,0,3)}- ${fn:substring(userInfo.userPhone,3,7)} - ${fn:substring(userInfo.userPhone,7,11)}</td>
									<td>${userInfo.userEmail}</td>
									<td><fmt:formatDate value="${userInfo.joinDate}" pattern="yyyy-MM-dd hh:mm:ss" /></td>
									<td><button type="button" onclick="location.href='/admin/userUpdate?userNum=${userInfo.userNum}'">수정</button>
										<button>삭제</button></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</section>

		<footer id="footer">
			<div id="footer_box"><%@ include file="../include/footer.jsp"%></div>
		</footer>
	</div>
</body>
</html>