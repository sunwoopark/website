<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<link rel="stylesheet" href="/resources/css/admin/userList.css" />


<html>
<head>
<title>Board Admin</title>

<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script>
	$(function() {
		$("#btnWrite").click(function() {
			location.href = "/admin/board/boardWrite";
		});
	});
	function list(page) {
		location.href = "/admin/board/boardList?curPage=" + page
				+ "&search_option=${map.search_option}&keyword=${map.keyword}";
	}
</script>

</head>
<body>

	<div id="root">
		<header id="header">
			<div id="header_box">
				<%@ include file="../include/header.jsp"%>
			</div>
		</header>
		<nav id="nav">
			<div id="nav_box"><%@ include file="../include/nav.jsp"%></div>
		</nav>

		<section id="container">
			<div id="aside">
				<%@ include file="../include/aside.jsp"%>
			</div>
			<div id="container_box">
				<div>
					<button type="button" id="btnWrite">게시글 작성</button>
				</div>

				<div>
					<form action="/admin/board/boardList" name="form1" method="post">
						<select name="search_option">
							<c:choose>
								<c:when test="${map.search_option == 'all' }">
									<option value="all" selected="selected">이름+내용+제목</option>
									<option value="userName">이름</option>
									<option value="bContent">내용</option>
									<option value="bTitle">제목</option>
								</c:when>
								<c:when test="${map.search_option == 'userName' }">
									<option value="all">이름+내용+제목</option>
									<option value="userName" selected="selected">이름</option>
									<option value="bContent">내용</option>
									<option value="bTitle">제목</option>
								</c:when>
								<c:when test="${map.search_option == 'bContent' }">
									<option value="all">이름+내용+제목</option>
									<option value="userName">이름</option>
									<option value="bContent" selected="selected">내용</option>
									<option value="bTitle">제목</option>
								</c:when>
								<c:when test="${map.search_option == 'bTitle' }">
									<option value="all">이름+내용+제목</option>
									<option value="userName">이름</option>
									<option value="bContent">내용</option>
									<option value="bTitle" selected="selected">제목</option>
								</c:when>
							</c:choose>
						</select> <input type="text" name="keyword" value="${map.keyword }"> <input type="submit" value="조회">
					</form>
				</div>

				<div>${map.count }개의게시물</div>
				<div class="memberInfo">
					<table>
						<colgroup>
							<col width="150px">
							<col width="350px">
							<col width="150px">
							<col width="150px">
							<col width="250px">
							<col width="250px">
							<col width="350px">
						</colgroup>
						<thead class="memberInfoTitle">
							<tr>
								<th>번호</th>
								<th>제목</th>
								<th>유저아이디</th>
								<th>유저이름</th>
								<th>날짜</th>
								<th>조회수</th>
								<th>기타</th>
							</tr>
						</thead>
						<tbody class="memberInfoContent">
							<c:forEach var="row" items="${map.list }">
								<tr>
									<td>${row.bNum}</td>
									<td><a
										href="/admin/board/boardView?bNum=${row.bNum}&curPage=${map.pager.curPage }&search_option=${map.search_option }&keyword=${map.keyword }">
											${row.bTitle}</a> <!-- 댓글개수 --> 
											<c:if test="${row.cnt > 0 }">
												<span style="color: red;">[${row.cnt}]</span>
											</c:if>
											</td>
									<td>${row.userId}</td>
									<td>${row.userName}</td>
									<td><fmt:formatDate value="${row.regdate }" pattern="yyyy-MM-dd HH:mm:ss" /></td>
									<td>${row.viewcnt }</td>
									<td><button type="button" onclick="location.href='/admin/board/boardUpdate?bNum=${row.bNum}'">수정</button>
										<button type="button" onclick="location.href='/admin/board/boardDelete?bNum=${row.bNum}'">삭제</button></td>
								</tr>
							</c:forEach>

							<!-- 페이지 네비게이션 -->
							<tr>
								<td colspan="7" align="center"><c:if test="${map.pager.curBlock > 1 }">
										<a href="javascript:list('1')">[처음]</a>
									</c:if> <c:if test="${map.pager.curBlock > 1 }">
										<a href="javascript:list('${map.pager.prevPage }')">[이전]</a>
									</c:if> <c:forEach var="num" begin="${map.pager.blockBegin }" end="${map.pager.blockEnd }">
										<c:choose>
											<c:when test="${num == map.pager.curPage }">
												<span style="color: red;">${num }</span>&nbsp;
											</c:when>
											<c:otherwise>
												<a href="javascript:list('${num }')">${num }</a>&nbsp;
											</c:otherwise>
										</c:choose>
									</c:forEach> <c:if test="${map.pager.curBlock <= map.pager.totBlock}">
										<a href="javascript:list('${map.pager.nextPage }')">[다음]</a>
									</c:if> <c:if test="${map.pager.curPage <= map.pager.totPage}">
										<a href="javascript:list('${map.pager.totPage }')">[끝]</a>
									</c:if></td>
							</tr>
						</tbody>
					</table>
				</div>

			</div>
		</section>

		<footer id="footer">
			<div id="footer_box"><%@ include file="../include/footer.jsp"%></div>
		</footer>
	</div>
</body>
</html>