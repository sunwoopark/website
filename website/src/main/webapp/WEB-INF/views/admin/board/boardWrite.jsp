<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<link rel="stylesheet" href="/resources/css/admin/userList.css" />


<html>
<head>
<title>Board Admin</title>

<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="/resources/js/include/go-back.js"></script>
</head>
<body>

	<div id="root">
		<header id="header">
			<div id="header_box">
				<%@ include file="../include/header.jsp"%>
			</div>
		</header>
		<nav id="nav">
			<div id="nav_box"><%@ include file="../include/nav.jsp"%></div>
		</nav>

		<section id="container">
			<div id="aside">
				<%@ include file="../include/aside.jsp"%>
			</div>
			<div id="container_box">
				<form action="/admin/board/boardWriteResult" id="form1" name="form1" method="post">
					<div>
						제목 <input type="text" name="bTitle" id="bTitle" size="80" placeholder="제목을 입력하세요.">
					</div>
					<div style="width: 800px;">
						내용
						<textarea id="bContent" name="bContent" rows="3" cols="80" placeholder="내용을 입력하세요."></textarea>
					</div>
					<div>
						첨부파일을 등록하세요.
						<div class="fileDrop"></div>
						<div id="uploadedList"></div>
					</div>
					<div style="width: 700px; text-align: center;">
						<button type="submit">확 인</button>
						<button type="button" class="go-back">취 소</button>
					</div>
				</form>
			</div>
		</section>

		<footer id="footer">
			<div id="footer_box"><%@ include file="../include/footer.jsp"%></div>
		</footer>
	</div>
</body>
</html>