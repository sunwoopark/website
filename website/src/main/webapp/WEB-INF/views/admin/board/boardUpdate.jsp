<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<html>
<head>
<title>Board Admin</title>

<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="/resources/js/include/go-back.js"></script>
<link rel="stylesheet" href="/resources/css/admin/board/boardView.css" />

</head>
<body>

	<div id="root">
		<header id="header">
			<div id="header_box">
				<%@ include file="../include/header.jsp"%>
			</div>
		</header>
		<nav id="nav">
			<div id="nav_box"><%@ include file="../include/nav.jsp"%></div>
		</nav>

		<section id="container">
			<div id="aside">
				<%@ include file="../include/aside.jsp"%>
			</div>
			<div id="container_box">
				<form action="/admin/board/boardUpdateResult" id="form1" name="form1" method="post">
					<div class="date">
						작성일자 :<span> <fmt:formatDate value="${dto.regdate }" pattern="yyyy-MM-dd a HH:mm:ss" /></span>
					</div>
					<div class="hit">
						조회수 : <span>${dto.viewcnt }</span>
					</div>
					<div class="name">
						이름 : <span> ${dto.userName }</span>
					</div>
					<div class="title">
						제목 : <span><input type="text" name="bTitle" value="${dto.bTitle }"></span>
					</div>
					<div class="content" >
						내용 : <span><textarea rows="5" cols="30" name="bContent" style="width: 1200px; height: 200px" >${dto.bContent }</textarea>
						</span>
					</div>
					<!-- 					<script>
						CKEDITOR.replace("content", {
							filebrowserUploadUrl : "/imageUpload",
							height : "500px"
						});
					</script> -->
					<div id="uploadedList"></div>
					<div class="fileDrop"></div>

					<hr>

					<div align="center">
						<input type="hidden" name="bNum" value="${dto.bNum }">
						<button type="submit" id="btnUpdate">수정</button>
						<button type="button" id="btnList" class="go-back">취소</button>
					</div>
				</form>

			</div>
		</section>

		<footer id="footer">
			<div id="footer_box"><%@ include file="../include/footer.jsp"%></div>
		</footer>
	</div>
</body>
</html>