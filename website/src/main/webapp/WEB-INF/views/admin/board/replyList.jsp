<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="/resources/css/admin/board/replyList.css" />
</head>
<body>
	<div style="margin-top: 30px;">
		<table style="width: 100%">
			<c:forEach var="row" items="${list }">
				<tr class="reply-box">
					<td class="reply-name">${row.userName }<br> (<fmt:formatDate value="${row.regdate }" pattern="yyyy-MM-dd HH:mm:ss" />)
					</td>
					<td class="reply-replytext">${row.replytext }</td>
					<td class="reply-btn flex-row-around"><button>수정</button><button>삭제</button></td>
				</tr>
			</c:forEach>
			<tr class="reply-page-btn">
				<td colspan="3" align="center" >
					<%-- <c:if test="${pager.curBlock > 1 }">
					<a href="javascript:replylist('1')">[처음]</a>
				</c:if>  --%> <c:if test="${pager.curBlock > 1 }">
						<a href="javascript:listReply('${pager.prevPage }')">[이전]</a>
					</c:if> <c:forEach var="num" begin="${pager.blockBegin }" end="${pager.blockEnd }">
						<c:choose>
							<c:when test="${num == pager.curPage }">
								<span style="color: red;">${num }</span>&nbsp;
						</c:when>
							<c:otherwise>
								<a href="javascript:listReply('${num }')">${num }</a>&nbsp;
						</c:otherwise>
						</c:choose>
					</c:forEach> <c:if test="${pager.curBlock <= pager.totBlock}">
						<a href="javascript:listReply('${pager.nextPage }')">[다음]</a>
					</c:if> <%-- <c:if test="${map.pager.curPage <= map.pager.totPage}">
					<a href="javascript:replyList('${pager.totPage }')">[끝]</a>
				</c:if>
				 --%>
				</td>
			</tr>
		</table>
	</div>
</body>
</html>