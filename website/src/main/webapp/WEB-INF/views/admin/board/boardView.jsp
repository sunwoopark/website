<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<html>
<head>
<title>Board Admin</title>

<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="/resources/js/include/go-back.js"></script>
<link rel="stylesheet" href="/resources/css/admin/board/boardView.css" />
<script>
	$(function() {
		listReply("1");
		$('#btnReply').click(function() {
			reply();
		});
	});
	function reply() {
		var replytext = $('#replytext').val();
		var bNum = "${dto.bNum}";
		var param = {
			"replytext" : replytext,
			"bNum" : bNum
		};

		$.ajax({
			type : "post",
			url : "/reply/replyInsert",
			data : param,
			success : function() {
				alert("댓글이 등록되었습니다.");
				listReply("1");
			}
		});
	}
	function listReply(num) {
		$.ajax({
			type : "post",
			url : "/reply/replyList?bNum=${dto.bNum}&curPage=" + num,
			success : function(result) {
				console.log(result);
				$("#listReply").html(result);
			}
		});
	}
</script>



</head>
<body>

	<div id="root">
		<header id="header">
			<div id="header_box">
				<%@ include file="../include/header.jsp"%>
			</div>
		</header>
		<nav id="nav">
			<div id="nav_box"><%@ include file="../include/nav.jsp"%></div>
		</nav>

		<section id="container">
			<div id="aside">
				<%@ include file="../include/aside.jsp"%>
			</div>
			<div id="container_box">
				<form id="form1" name="form1" method="post">
					<div class="date">
						작성일자 :<span> <fmt:formatDate value="${dto.regdate }" pattern="yyyy-MM-dd a HH:mm:ss" /></span>
					</div>
					<div class="hit">
						조회수 : <span>${dto.viewcnt }</span>
					</div>
					<div class="name">
						이름 : <span>${dto.userName }</span>
					</div>
					<div class="title">
						제목 : <span>${dto.bTitle }</span>
					</div>
					<div class="content">
						내용 : <span>${dto.bContent }</span>
					</div>
					<!-- 					<script>
						CKEDITOR.replace("content", {
							filebrowserUploadUrl : "/imageUpload",
							height : "500px"
						});
					</script> -->
					<div id="uploadedList"></div>
					<div class="fileDrop"></div>

					<hr>

					<!-- 댓글 쓰기 -->
					<div style="width: 700px; text-align: center;" class="replyarea flex-row-between">
						<c:if test="${sessionScope.userId != null }">
							<div>
								<textarea rows="5" cols="80" id="replytext" placeholder="댓글을 작성하세요"></textarea>
							</div>
							<div>
								<button type="button" id="btnReply">댓글쓰기</button>
							</div>
						</c:if>
					</div>
					<div id="listReply"></div>

					<div align="center">
						<input type="hidden" name="bNum" value="${dto.bNum }">
						<button type="button" id="btnUpdate" onclick="location.href='/admin/board/boardUpdate?bNum=${dto.bNum}'">수정</button>
						<button type="button" id="btnDelete" onclick="location.href='/admin/board/boardDelete?bNum=${dto.bNum}'">삭제</button>
						<button type="button" id="btnList" class="go-back">목록</button>
					</div>
				</form>

			</div>
		</section>

		<footer id="footer">
			<div id="footer_box"><%@ include file="../include/footer.jsp"%></div>
		</footer>
	</div>
</body>
</html>