<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Insert title here</title>
<link rel="stylesheet" href="/resources/css/mypage/board-list.css" />

<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script>
	function list(page) {
		location.href = "/mypage/boardList?curPage=" + page
				+ "&search_option=${map.search_option}&keyword=${map.keyword}";
	}
</script>

</head>

<body>

	<div class="wrap">
		<!-- header 페이지 -->
		<%@ include file="../include/header.jsp"%>

		<!-- gnb 페이지 -->
		<%@ include file="../include/gnb.jsp"%>

		<!-- content 페이지 -->
		<div class="content">
			<div class="mypage flex-row-between">
				<%@ include file="include/mypageSideMenu.jsp"%>
				<div class="board-list">
					<table>
						<colgroup>
							<col width="100px">
							<col width="400px">
							<col width="200px">
							<col width="200px">
							<col width="200px">
						</colgroup>
						<tr class="board-title">
							<td>번호</td>
							<td>제목</td>
							<td>이름</td>
							<td>날짜</td>
							<td>조회수</td>
						</tr>
						<c:forEach var="row" items="${map.list }">
							<tr class="board-content">
								<td>${row.bNum}</td>
								<td><a
									href="/mypage/boardView?bNum=${row.bNum}&curPage=${map.pager.curPage }&search_option=${map.search_option }&keyword=${map.keyword }">${row.bTitle}</a>
									<!-- 댓글개수 --> <c:if test="${row.cnt > 0 }">
										<span style="color: red;">[${row.cnt}]</span>
									</c:if></td>
								<td>${row.userName}</td>
								<td><fmt:formatDate value="${row.regdate }" pattern="yyyy-MM-dd HH:mm:ss" /></td>
								<td>${row.viewcnt }</td>
							</tr>
						</c:forEach>
						<!-- 페이지 네비게이션 -->
						<tr class="board-reply">
							<td colspan="5" align="center"><c:if test="${map.pager.curBlock > 1 }">
									<a href="javascript:list('1')">[처음]</a>
								</c:if> <c:if test="${map.pager.curBlock > 1 }">
									<a href="javascript:list('${map.pager.prevPage }')">[이전]</a>
								</c:if> <c:forEach var="num" begin="${map.pager.blockBegin }" end="${map.pager.blockEnd }">
									<c:choose>
										<c:when test="${num == map.pager.curPage }">
											<span style="color: red;">${num }</span>&nbsp;
											</c:when>
										<c:otherwise>
											<a href="javascript:list('${num }')">${num }</a>&nbsp;
											</c:otherwise>
									</c:choose>
								</c:forEach> <c:if test="${map.pager.curBlock <= map.pager.totBlock}">
									<a href="javascript:list('${map.pager.nextPage }')">[다음]</a>
								</c:if> <c:if test="${map.pager.curPage <= map.pager.totPage}">
									<a href="javascript:list('${map.pager.totPage }')">[끝]</a>
								</c:if></td>
						</tr>
					</table>
				</div>
			</div>
		</div>


		<!-- footer 페이지 -->
		<%@ include file="../include/footer.jsp"%>
	</div>
</body>

</html>