<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>박박박꽃배달</title>
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="/resources/css/mypage/index.css" />


</head>
<body>

	<div class="wrap">
		<!-- header 페이지 -->
		<%@ include file="../include/header.jsp"%>

		<!-- gnb 페이지 -->
		<%@ include file="../include/gnb.jsp"%>

		<!-- content 페이지 -->
		<div class="content">
			<div class="mypage flex-row-between">
				<%@ include file="include/mypageSideMenu.jsp"%>
				<div class="mypage-box2">
					<div class="mypage-info">
						<div class="mypage-info-box">
							<img src="http://placehold.it/950x150" alt="">
						</div>
						<div class="mypage-info-box2" style="margin-top: 20px">
							<img src="http://placehold.it/950x500" alt="">
						</div>
					</div>
				</div>
			</div>
		</div>


		<!-- footer 페이지 -->
		<%@ include file="../include/footer.jsp"%>
	</div>


</body>
</html>
