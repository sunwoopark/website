<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet" href="/resources/css/mypage/index.css" />

	<div class="mypage-box1">
		<div class="mypage-nav">
			<p>
				<a href="/mypage/index">마이페이지</a>
			</p>
			<ul>
				<li><a href="/order/orderList">주문/배송</a></li>
				<li><a href="/">적립금</a></li>
				<li><a href="/">쿠폰</a></li>
				<li><a href="/">기념일관리</a></li>
				<li><a href="/">1:1문의</a></li>
				<li><a href="/">자주묻는 질문</a></li>	<!-- mypage/mypageFag -->
				<li><a href="/mypage/boardList">공지사항</a></li>
				<li><a href="/mypage/memberModform">회원정보수정</a></li>
				<li><a href="/mypage/memberWithDraw">회원탈퇴</a></li>
			</ul>
		</div>
		<div class="mypage-bar"></div>
		<div class="callcenter-box">
			<img src="http://placehold.it/180x300" alt="">
		</div>
	</div>
