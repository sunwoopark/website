<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Insert title here</title>
<link rel="stylesheet" href="/resources/css/mypage/board-view.css" />

<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="/resources/js/include/go-back.js"></script>
<script>
	function list(page) {
		location.href = "/mypage/boardList?curPage=" + page
				+ "&search_option=${map.search_option}&keyword=${map.keyword}";
	}
</script>
<script>
	$(function() {
		listReply("1");
		$('#btnReply').click(function() {
			reply();
		});
	});
	function reply() {
		var replytext = $('#replytext').val();
		var bNum = "${dto.bNum}";
		var param = {
			"replytext" : replytext,
			"bNum" : bNum
		};

		$.ajax({
			type : "post",
			url : "/reply/replyInsert",
			data : param,
			success : function() {
				alert("댓글이 등록되었습니다.");
				listReply("1");
			}
		});
	}
	function listReply(num) {
		$.ajax({
			type : "post",
			url : "/reply/replyList?bNum=${dto.bNum}&curPage=" + num,
			success : function(result) {
				console.log(result);
				$("#listReply").html(result);
			}
		});
	}
</script>

</head>

<body>

	<div class="wrap">
		<!-- header 페이지 -->
		<%@ include file="../include/header.jsp"%>

		<!-- gnb 페이지 -->
		<%@ include file="../include/gnb.jsp"%>

		<!-- content 페이지 -->
		<div class="content">
			<div class="mypage flex-row-between">
				<%@ include file="include/mypageSideMenu.jsp"%>
				<div class="board-view">
					<table>
						<colgroup>
							<col width="100px">
							<col width="400px">
							<col width="200px">
							<col width="200px">
							<col width="200px">
						</colgroup>
						<tr class="view-name">
							<td>번호</td>
							<td>제목</td>
							<td>이름</td>
							<td>날짜</td>
							<td>조회수</td>
						</tr>
						<tr class="view-content">
							<td>${dto.bNum }</td>
							<td>${dto.bTitle }</td>
							<td>${dto.userName }</td>
							<td><fmt:formatDate value="${dto.regdate }" pattern="yyyy-MM-dd" /></td>
							<td>${dto.viewcnt }</td>
						</tr>
						<tr>
							<td class="bContent" colspan="5">${dto.bContent }</td>
						</tr>
						<tr>
							<td colspan="5" class="btn">
								<button class="go-back">목록</button>
							</td>
						</tr>
					</table>

					<hr>

					<!-- 댓글 쓰기 -->
					<div style="width: 700px; text-align: center;" class="replyarea flex-row-between">
						<c:if test="${sessionScope.userId != null }">
							<div>
								<textarea rows="5" cols="80" id="replytext" placeholder="댓글을 작성하세요"></textarea>
							</div>
							<div>
								<button type="button" id="btnReply">댓글쓰기</button>
							</div>
						</c:if>
					</div>
					<div id="listReply"></div>


				</div>
			</div>
		</div>


		<!-- footer 페이지 -->
		<%@ include file="../include/footer.jsp"%>
	</div>
</body>

</html>