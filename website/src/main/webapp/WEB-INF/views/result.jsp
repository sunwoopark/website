<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<html>
<head>
<title>박박박꽃배달</title>
</head>
<body>

	<c:if test="${message == 'joinOk' }">
		<script>
			alert("회원가입이 되었습니다.");
			location.href = "/"
		</script>
	</c:if>

	<c:if test="${message == 'loginOk' }">
		<script>
			location.href = "/"
		</script>
	</c:if>

	<c:if test="${message == 'loginNo' }">
		<script>
			alert("로그인 이후 사용이 가능합니다.");
			location.href = "/member/login"
		</script>
	</c:if>

	<c:if test="${message == 'cartinsert' }">
		<script>
			confirm("장바구니로 이동하시겠습니까?");
			location.href = "/cart/list"
		</script>
	</c:if>


	<c:if test="${message == 'error' }">
		<script>
			alert("잘못된 경로 입니다.");
			location.href = "/"
		</script>
	</c:if>



</body>
</html>
