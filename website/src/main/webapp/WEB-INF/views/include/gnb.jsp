<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<link rel="stylesheet" href="/resources/css/include/gnb.css" />

<div class="gnb">
	<div class="gnb-wrap">
		<div class="gnb-top">
			<ul class="flex-row-around">
				<li class="gnb-menu01"><a href="/shop/list?c=1">꽃다발</a></li>
				<div class="gnb-bar"></div>
				<li class="gnb-menu01"><a href="/shop/list?c=2">꽃바구니</a></li>
				<div class="gnb-bar"></div>
				<li class="gnb-menu01"><a href="/shop/list?c=3">꽃상자</a></li>
				<div class="gnb-bar"></div>
				<li class="gnb-menu01"><a href="/shop/list?c=4">축하화환</a></li>
				<div class="gnb-bar"></div>
				<li class="gnb-menu01"><a href="/shop/list?c=5">근조화환</a></li>
				<div class="gnb-bar"></div>
				<li class="gnb-menu01"><a href="/shop/list?c=6">동양란</a></li>
				<div class="gnb-bar"></div>
				<li class="gnb-menu01"><a href="/shop/list?c=7">서양란</a></li>
				<div class="gnb-bar"></div>
				<li class="gnb-menu01"><a href="/shop/list?c=8">공기정화식물</a></li>
				<li class="gnb-menu02"><a href="/shop/list?c=9">★시즌상품</a></li>
			</ul>
		</div>
	</div>
</div>
