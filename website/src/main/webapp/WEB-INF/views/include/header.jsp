<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<link rel="stylesheet" href="/resources/css/include/header.css" />

<link rel="stylesheet" href="/resources/css/reset.css" />
<link rel="stylesheet" href="/resources/css/reset-add.css" />

<div class="header">
	<div class="header-wrap">
		<div class="header-top flex-row-between">
			<div class="bookmark">즐겨찾기</div>
			<div class="login-wrap">
				<c:if test="${userId == null}">
					<a href="/member/login">로그인</a>
					<span>&nbsp;|&nbsp;</span>
					<a href="/member/join">회원가입</a>
					<span>&nbsp;|&nbsp;</span>
				</c:if>
				<c:if test="${userId != null }">
					<c:if test="${userId.verify == 9 }">
						<a href="/admin/index" style="color: red; font-weight: bold;">[관리자 화면으로 이동]</a>
						<span>&nbsp;|&nbsp;</span>
					</c:if>
					<a href="/member/logout">로그아웃</a>
					<span>&nbsp;|&nbsp;</span>
				</c:if>
				<a href="/cart/list">장바구니</a> <span>&nbsp;&nbsp;|&nbsp;</span> <a href="/order/orderList">주문/배송조회</a> <span>&nbsp;&nbsp;|&nbsp;</span> <a
					href="/mypage/index">마이페이지</a>
			</div>
		</div>
		<div class="header-bottom flex-row-between">
			<div class="header-logo">
				<a href="/"> <img src="/resources/images/home/header/logo.jpg" alt="로고" />
				</a>
			</div>
			<div class="header-search">
				<form action="/shop/listSearch" method="get">
					<input type="text" class="main-search" name="keyword">
					<button type="submit" class="header-search-btn">
						<img src="/resources/images/home/header/search.jpg" />
					</button>
				</form>
			</div>
			<div class="header-tel">
				<!-- <img src="http://placehold.it/130x50" alt="#" /> -->
			</div>
			<div class="header-side">
				<img src="/resources/images/home/header/tel.jpg" alt="#" />
			</div>
		</div>
	</div>
</div>
