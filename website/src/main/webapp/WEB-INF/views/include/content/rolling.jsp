<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<link rel="stylesheet" href="/resources/css/include/content/rolling.css" />
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="/resources/js/include/rolling.js"></script>

<div class="slideshow">
	<div class="slidesshow_slides">
		<a href="">
			<img src="resources/images/home/rolling/rolling_01.jpg" alt="slide1" width="2000px" height="500px">
		</a>
		<a href="">
			<img src="resources/images/home/rolling/rolling_02.jpg" alt="slide1" width="2000px" height="500px">
		</a>
		<a href="">
			<img src="resources/images/home/rolling/rolling_03.jpg" alt="slide1" width="2000px" height="500px">
		</a>
		<a href="">
			<img src="resources/images/home/rolling/rolling_04.jpg" alt="slide1" width="2000px" height="500px">
		</a>
		<a href="">
			<img src="resources/images/home/rolling/rolling_05.jpg" alt="slide1" width="2000px" height="500px">
		</a>
	</div>

	<div class="slidershow_nav">
		<a href="" class="prev">prev</a>
		<a href="" class="next">next</a>
	</div>

	<div class="indicator"></div>
</div>