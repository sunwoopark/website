<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<link rel="stylesheet" href="/resources/css/include/content/best.css" />

<div class="best-wrap">
	<div class="best-title">
		<img src="\resources\images\home\best\main_title02.png" alt="" width="1200" height="79" />

	</div>

	<div class="box01 flex-row-between">
		<div class="best-box1">
			<div class="best-box1-fir">
				<p class="box1-title">사랑.고백 BEST</p>
				<c:forEach var="m1" items="${m.Map1.best }" begin="0" end="0">
					<a href="/shop/view?n=${m1.productId }"> <img src="/resources/images/${m1.pictureUrl }" alt="" width="360" height="360" />
					</a>
				</c:forEach>
				<div class="box1-content">
					<p>꽃같은 그녀에게 전하는 선물</p>
					<p>사랑하는 마음을 담은 향기로운 꽃을 선물하세요.</p>
					<br> <br> <span>#프로포즈의정석 #꽃길만걷자</span>
				</div>
			</div>
			<div class="best-box-bar"></div>
			<div class="best-box1-sec flex-row-around">
				<c:forEach var="m1" items="${m.Map1.best }" begin="1" end="1">
					<div>
						<p>2위</p>
					</div>
					<div>
						<a href="/shop/view?n=${m1.productId }"> <img src="/resources/images/${m1.pictureUrl }" alt="" width="110" height="110">
						</a>
					</div>
					<div>
						<div class="box1-sec-content">
							<div>${m1.productName }</div>
							<div>${m1.price }</div>
						</div>
					</div>
				</c:forEach>
			</div>
			<div class="best-box-bar"></div>

			<div class="best-box1-thi flex-row-around">
				<c:forEach var="m1" items="${m.Map1.best }" begin="2" end="2">
					<div>
						<p>3위</p>
					</div>
					<div>
						<a href="/shop/view?n=${m1.productId }"> <img src="/resources/images/${m1.pictureUrl}" alt="" width="110" height="110">
						</a>
					</div>
					<div>
						<div class="box1-thi-content">
							<div>${m1.productName }</div>
							<div>${m1.price }</div>
							<!-- <span>회원가 : 123,000 원</span> -->
						</div>
					</div>
				</c:forEach>
			</div>
			<div class="item-go">
				<a href="#">사랑.고백 바로가기</a>
			</div>
		</div>
		<!--  -->
		<div class="best-box1">
			<div class="best-box1-fir">
				<p class="box1-title">생일.기념일 BEST</p>
				<c:forEach var="m1" items="${m.Map2.best }" begin="0" end="0">
					<a href="/shop/view?n=${m1.productId }"> <img src="/resources/images/${m1.pictureUrl }" alt="" width="360" height="360" />
					</a>
				</c:forEach>
				<div class="box1-content">
					<p>축하하고 싶은 날,</p>
					<p>당일배송 꽃배달로 서프라이즈 이벤트☆</p>
					<br> <br> <span>#두배의 감동을 전하세요!</span>
				</div>
			</div>
			<div class="best-box-bar"></div>
			<div class="best-box1-sec flex-row-around">
				<c:forEach var="m1" items="${m.Map2.best }" begin="1" end="1">
					<div>
						<p>2위</p>
					</div>
					<div>
						<a href="/shop/view?n=${m1.productId }"> <img src="/resources/images/${m1.pictureUrl }" alt="" width="110" height="110">
						</a>
					</div>
					<div>
						<div class="box1-sec-content">
							<div>${m1.productName }</div>
							<div>${m1.price }</div>
							<!--  <span>회원가 : 123,000 원</span> -->
						</div>
					</div>
				</c:forEach>
			</div>
			<div class="best-box-bar"></div>

			<div class="best-box1-thi flex-row-around">
				<c:forEach var="m1" items="${m.Map2.best }" begin="2" end="2">
					<div>
						<p>3위</p>
					</div>
					<div>
						<a href="/shop/view?n=${m1.productId }"> <img src="/resources/images/${m1.pictureUrl}" alt="" width="110" height="110">
						</a>
					</div>
					<div>
						<div class="box1-thi-content">
							<div>${m1.productName }</div>
							<div>${m1.price }</div>

						</div>
					</div>
				</c:forEach>
			</div>
			<div class="item-go">
				<a href="#">생일.기념일 바로가기</a>
			</div>
		</div>
		<!--  -->
		<div class="best-box1">
			<div class="best-box1-fir">
				<p class="box1-title">결혼.축하 BEST</p>
				<c:forEach var="m1" items="${m.Map4.best }" begin="0" end="0">
					<a href="/shop/view?n=${m1.productId }"> <img src="/resources/images/${m1.pictureUrl }" alt="" width="360" height="360" />
					</a>
				</c:forEach>
				<div class="box1-content">
					<p>모든 축하자리에 디자인 화환으로 자리를 빛내주세요.</p>
					<p>기쁨이 두 배가 됩니다!</p>
					<br> <br> <span>#결혼식 #개업 #전시회</span>
				</div>
			</div>
			<div class="best-box-bar"></div>
			<div class="best-box1-sec flex-row-around">
				<c:forEach var="m1" items="${m.Map4.best }" begin="1" end="1">
					<div>
						<p>2위</p>
					</div>
					<div>
						<a href="/shop/view?n=${m1.productId }"> <img src="/resources/images/${m1.pictureUrl }" alt="" width="110" height="110">
						</a>
					</div>
					<div>
						<div class="box1-sec-content">
							<div>${m1.productName }</div>
							<div>${m1.price }</div>
							<!--  <span>회원가 : 123,000 원</span> -->
						</div>
					</div>
				</c:forEach>
			</div>
			<div class="best-box-bar"></div>

			<div class="best-box1-thi flex-row-around">
				<c:forEach var="m1" items="${m.Map4.best }" begin="2" end="2">
					<div>
						<p>3위</p>
					</div>
					<div>
						<a href="/shop/view?n=${m1.productId }"> <img src="/resources/images/${m1.pictureUrl}" alt="" width="110" height="110">
						</a>
					</div>
					<div>
						<div class="box1-thi-content">
							<div>${m1.productName }</div>
							<div>${m1.price }</div>
							<!-- <span>회원가 : 123,000 원</span> -->
						</div>
					</div>
				</c:forEach>
			</div>
			<div class="item-go">
				<a href="#">결혼.축하 바로가기</a>
			</div>
		</div>
	</div>

	<div class="box02 flex-row-between">
		<div class="best-box1">
			<div class="best-box1-fir">
				<p class="box1-title">조문.추모 BEST</p>
				<c:forEach var="m1" items="${m.Map5.best }" begin="0" end="0">
					<a href="/shop/view?n=${m1.productId }"> <img src="/resources/images/${m1.pictureUrl }" alt="" width="360" height="360" />
					</a>
				</c:forEach>
				<div class="box1-content">
					<p>장례식장에 보내는 정성 고인의 대한</p>
					<p>애도의 마음을 담아 정성으로 보내드립니다.</p>
					<br> <br> <span>#장례식장꽃 #명복을빕니다</span>
				</div>
			</div>
			<div class="best-box-bar"></div>
			<div class="best-box1-sec flex-row-around">
				<c:forEach var="m1" items="${m.Map5.best }" begin="1" end="1">
					<div>
						<p>2위</p>
					</div>
					<div>
						<a href="/shop/view?n=${m1.productId }"> <img src="/resources/images/${m1.pictureUrl }" alt="" width="110" height="110">
						</a>
					</div>
					<div>
						<div class="box1-sec-content">
							<div>${m1.productName }</div>
							<div>${m1.price }</div>
							<!--  <span>회원가 : 123,000 원</span> -->
						</div>
					</div>
				</c:forEach>
			</div>
			<div class="best-box-bar"></div>

			<div class="best-box1-thi flex-row-around">
				<c:forEach var="m1" items="${m.Map5.best }" begin="2" end="2">
					<div>
						<p>3위</p>
					</div>
					<div>
						<a href="/shop/view?n=${m1.productId }"> <img src="/resources/images/${m1.pictureUrl}" alt="" width="110" height="110">
						</a>
					</div>
					<div>
						<div class="box1-thi-content">
							<div>${m1.productName }</div>
							<div>${m1.price }</div>
							<!-- <span>회원가 : 123,000 원</span> -->
						</div>
					</div>
				</c:forEach>
			</div>
			<div class="item-go">
				<a href="#">근조.추모 바로가기</a>
			</div>
		</div>
		<!--  -->
		<div class="best-box1">
			<div class="best-box1-fir">
				<p class="box1-title">승진.취임 BEST</p>
				<c:forEach var="m1" items="${m.Map6.best }" begin="0" end="0">
					<a href="/shop/view?n=${m1.productId }"> <img src="/resources/images/${m1.pictureUrl }" alt="" width="360" height="360" />
					</a>
				</c:forEach>
				<div class="box1-content">
					<p>소중한 분께 전하는 첫번째 정성</p>
					<p>소중한 분께 전하는 첫번째 정성 아름다운 마무리와 새로운 시작을 응원합니다.</p>
					<br> <br> <span>#품격을 #높이는 #동.서양란</span>
				</div>
			</div>
			<div class="best-box-bar"></div>
			<div class="best-box1-sec flex-row-around">
				<c:forEach var="m1" items="${m.Map6.best }" begin="1" end="1">
					<div>
						<p>2위</p>
					</div>
					<div>
						<a href="/shop/view?n=${m1.productId }"> <img src="/resources/images/${m1.pictureUrl }" alt="" width="110" height="110">
						</a>
					</div>
					<div>
						<div class="box1-sec-content">
							<div>${m1.productName }</div>
							<div>${m1.price }</div>
							<!--  <span>회원가 : 123,000 원</span> -->
						</div>
					</div>
				</c:forEach>
			</div>
			<div class="best-box-bar"></div>

			<div class="best-box1-thi flex-row-around">
				<c:forEach var="m1" items="${m.Map6.best }" begin="2" end="2">
					<div>
						<p>3위</p>
					</div>
					<div>
						<a href="/shop/view?n=${m1.productId }"> <img src="/resources/images/${m1.pictureUrl}" alt="" width="110" height="110">
						</a>
					</div>
					<div>
						<div class="box1-thi-content">
							<div>${m1.productName }</div>
							<div>${m1.price }</div>
							<!-- <span>회원가 : 123,000 원</span> -->
						</div>
					</div>
				</c:forEach>
			</div>
			<div class="item-go">
				<a href="#">승진.취임 바로가기</a>
			</div>
		</div>
		<!--  -->
		<div class="best-box1">
			<div class="best-box1-fir">
				<p class="box1-title">개업.이전 BEST</p>
				<c:forEach var="m1" items="${m.Map8.best }" begin="0" end="0">
					<a href="/shop/view?n=${m1.productId }"> <img src="/resources/images/${m1.pictureUrl }" alt="" width="360" height="360" />
					</a>
				</c:forEach>
				<div class="box1-content">
					<p>카페.사무실.거실등 인테리어까지 가능한</p>
					<p>실속있는 화분선물 하세요.</p>
					<br> <br> <span>#개업식 #대박 #흥해라</span>
				</div>
			</div>
			<div class="best-box-bar"></div>
			<div class="best-box1-sec flex-row-around">
				<c:forEach var="m1" items="${m.Map8.best }" begin="1" end="1">
					<div>
						<p>2위</p>
					</div>
					<div>
						<a href="/shop/view?n=${m1.productId }"> <img src="/resources/images/${m1.pictureUrl }" alt="" width="110" height="110">
						</a>
					</div>
					<div>
						<div class="box1-sec-content">
							<div>${m1.productName }</div>
							<div>${m1.price }</div>
							<!--  <span>회원가 : 123,000 원</span> -->
						</div>
					</div>
				</c:forEach>
			</div>
			<div class="best-box-bar"></div>

			<div class="best-box1-thi flex-row-around">
				<c:forEach var="m1" items="${m.Map8.best }" begin="2" end="2">
					<div>
						<p>3위</p>
					</div>
					<div>
						<a href="/shop/view?n=${m1.productId }"> <img src="/resources/images/${m1.pictureUrl}" alt="" width="110" height="110">
						</a>
					</div>
					<div>
						<div class="box1-thi-content">
							<div>${m1.productName }</div>
							<div>${m1.price }</div>
							<!-- <span>회원가 : 123,000 원</span> -->
						</div>
					</div>
				</c:forEach>
			</div>
			<div class="item-go">
				<a href="#"> 개업.이전 바로가기</a>
			</div>
		</div>
	</div>




</div>