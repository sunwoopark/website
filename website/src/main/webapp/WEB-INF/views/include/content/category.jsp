<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<link rel="stylesheet" href="/resources/css/include/content/category.css" />

<div class="category-wrap">
	<div class="category-title">
		<a href="#"> <img src="\resources\images\home\category\main_title03.png" alt="" width="1200px" height="100px">
		</a>
	</div>

	<div class="box01 flex-row-around">

		<div class="category-box1">
			<div class="box-title">
				<ul>
					<li style="font-size: 15px; font-weight: bold;">꽃다발|BEST</li>
					<li>
						<div class="title-bar"></div>
					</li>
					<li>
						<p>생일 | 이벤트 | 기념일 | 고백</p>
					</li>
				</ul>
			</div>

			<div class="box-list">
				<c:forEach var="m2" items="${m.Map1.category }" begin="0" end="0">
					<div class="box-list-fir">
						<a href="/shop/view?n=${m2.productId }"> <img src="/resources/images/${m2.pictureUrl }" alt="#" width="270" height="270" />
							<ul>
								<li>${m2.productName }</li>
								<li><fmt:formatNumber value="${m2.price }" pattern="###,###,###"></fmt:formatNumber></li>
							</ul>
						</a>
					</div>
				</c:forEach>
				<div class="flex-row-around">
					<c:forEach var="m2" items="${m.Map1.category }" begin="1" end="2">
						<div class="box-list-sec">
							<a href="/shop/view?n=${m2.productId }"> <img src="/resources/images/${m2.pictureUrl }" alt="#" width="150" height="150" />
								<ul>
									<li>${m2.productName }</li>
									<li><fmt:formatNumber value="${m2.price }" pattern="###,###,###"></fmt:formatNumber></li>
								</ul>
							</a>
						</div>
					</c:forEach>
				</div>
			</div>

			<div class="item-go">
				<a href="#">바로가기 →</a>
			</div>
		</div>
		
		<div class="category-box2">
			<div class="box-title">
				<ul>
					<li style="font-size: 15px; font-weight: bold;">꽃바구니|BEST</li>
					<li>
						<div class="title-bar"></div>
					</li>
					<li>
						<p>생일 | 이벤트 | 기념일 | 고백</p>
					</li>
				</ul>
			</div>

			<div class="box-list">
				<c:forEach var="m2" items="${m.Map2.category }" begin="0" end="0">
					<div class="box-list-fir">
						<a href="/shop/view?n=${m2.productId }"> <img src="/resources/images/${m2.pictureUrl }" alt="#" width="270" height="270" />
							<ul>
								<li>${m2.productName }</li>
								<li><fmt:formatNumber value="${m2.price }" pattern="###,###,###"></fmt:formatNumber></li>
							</ul>
						</a>
					</div>
				</c:forEach>
				<div class="flex-row-around">
					<c:forEach var="m2" items="${m.Map2.category }" begin="1" end="2">
						<div class="box-list-sec">
							<a href="/shop/view?n=${m2.productId }"> <img src="/resources/images/${m2.pictureUrl }" alt="#" width="150" height="150" />
								<ul>
									<li>${m2.productName }</li>
									<li><fmt:formatNumber value="${m2.price }" pattern="###,###,###"></fmt:formatNumber></li>
								</ul>
							</a>
						</div>
					</c:forEach>
				</div>
			</div>

			<div class="item-go">
				<a href="#">바로가기 →</a>
			</div>
		</div>
		
		<div class="category-box3">
			<div class="box-title">
				<ul>
					<li style="font-size: 15px; font-weight: bold;">꽃상자|BEST</li>
					<li>
						<div class="title-bar"></div>
					</li>
					<li>
						<p>생일 | 졸업식 | 기념일 | 프로포즈</p>
					</li>
				</ul>
			</div>

			<div class="box-list">
				<c:forEach var="m2" items="${m.Map3.category }" begin="0" end="0">
					<div class="box-list-fir">
						<a href="/shop/view?n=${m2.productId }"> <img src="/resources/images/${m2.pictureUrl }" alt="#" width="270" height="270" />
							<ul>
								<li>${m2.productName }</li>
								<li><fmt:formatNumber value="${m2.price }" pattern="###,###,###"></fmt:formatNumber></li>
							</ul>
						</a>
					</div>
				</c:forEach>
				<div class="flex-row-around">
					<c:forEach var="m2" items="${m.Map3.category }" begin="1" end="2">
						<div class="box-list-sec">
							<a href="/shop/view?n=${m2.productId }"> <img src="/resources/images/${m2.pictureUrl }" alt="#" width="150" height="150" />
								<ul>
									<li>${m2.productName }</li>
									<li><fmt:formatNumber value="${m2.price }" pattern="###,###,###"></fmt:formatNumber></li>
								</ul>
							</a>
						</div>
					</c:forEach>
				</div>
			</div>

			<div class="item-go">
				<a href="#">바로가기 →</a>
			</div>
		</div>


	</div>
	
	<div class="box02 flex-row-around">

		<div class="category-box4">
			<div class="box-title">
				<ul>
					<li style="font-size: 15px; font-weight: bold;">축하화환|BEST</li>
					<li>
						<div class="title-bar"></div>
					</li>
					<li>
						<p>결혼식 | 개업 | 전시회 | 이전</p>
					</li>
				</ul>
			</div>

			<div class="box-list">
				<c:forEach var="m2" items="${m.Map4.category }" begin="0" end="0">
					<div class="box-list-fir">
						<a href="/shop/view?n=${m2.productId }"> <img src="/resources/images/${m2.pictureUrl }" alt="#" width="270" height="270" />
							<ul>
								<li>${m2.productName }</li>
								<li><fmt:formatNumber value="${m2.price }" pattern="###,###,###"></fmt:formatNumber></li>
							</ul>
						</a>
					</div>
				</c:forEach>
				<div class="flex-row-around">
					<c:forEach var="m2" items="${m.Map4.category }" begin="1" end="2">
						<div class="box-list-sec">
							<a href="/shop/view?n=${m2.productId }"> <img src="/resources/images/${m2.pictureUrl }" alt="#" width="150" height="150" />
								<ul>
									<li>${m2.productName }</li>
									<li><fmt:formatNumber value="${m2.price }" pattern="###,###,###"></fmt:formatNumber></li>
								</ul>
							</a>
						</div>
					</c:forEach>
				</div>
			</div>

			<div class="item-go">
				<a href="#">바로가기 →</a>
			</div>
		</div>
		
		<div class="category-box5">
			<div class="box-title">
				<ul>
					<li style="font-size: 15px; font-weight: bold;">근조화환|BEST</li>
					<li>
						<div class="title-bar"></div>
					</li>
					<li>
						<p>장례식 | 조문</p>
					</li>
				</ul>
			</div>

			<div class="box-list">
				<c:forEach var="m2" items="${m.Map5.category }" begin="0" end="0">
					<div class="box-list-fir">
						<a href="/shop/view?n=${m2.productId }"> <img src="/resources/images/${m2.pictureUrl }" alt="#" width="270" height="270" />
							<ul>
								<li>${m2.productName }</li>
								<li><fmt:formatNumber value="${m2.price }" pattern="###,###,###"></fmt:formatNumber></li>
							</ul>
						</a>
					</div>
				</c:forEach>
				<div class="flex-row-around">
					<c:forEach var="m2" items="${m.Map5.category }" begin="1" end="2">
						<div class="box-list-sec">
							<a href="/shop/view?n=${m2.productId }"> <img src="/resources/images/${m2.pictureUrl }" alt="#" width="150" height="150" />
								<ul>
									<li>${m2.productName }</li>
									<li><fmt:formatNumber value="${m2.price }" pattern="###,###,###"></fmt:formatNumber></li>
								</ul>
							</a>
						</div>
					</c:forEach>
				</div>
			</div>

			<div class="item-go">
				<a href="#">바로가기 →</a>
			</div>
		</div>
		
		<div class="category-box6">
			<div class="box-title">
				<ul>
					<li style="font-size: 15px; font-weight: bold;">관엽식물|BEST</li>
					<li>
						<div class="title-bar"></div>
					</li>
					<li>
						<p>개업 | 이전 | 전시회 | 인테리어</p>
					</li>
				</ul>
			</div>

			<div class="box-list">
				<c:forEach var="m2" items="${m.Map6.category }" begin="0" end="0">
					<div class="box-list-fir">
						<a href="/shop/view?n=${m2.productId }"> <img src="/resources/images/${m2.pictureUrl }" alt="#" width="270" height="270" />
							<ul>
								<li>${m2.productName }</li>
								<li><fmt:formatNumber value="${m2.price }" pattern="###,###,###"></fmt:formatNumber></li>
							</ul>
						</a>
					</div>
				</c:forEach>
				<div class="flex-row-around">
					<c:forEach var="m2" items="${m.Map6.category }" begin="1" end="2">
						<div class="box-list-sec">
							<a href="/shop/view?n=${m2.productId }"> <img src="/resources/images/${m2.pictureUrl }" alt="#" width="150" height="150" />
								<ul>
									<li>${m2.productName }</li>
									<li><fmt:formatNumber value="${m2.price }" pattern="###,###,###"></fmt:formatNumber></li>
								</ul>
							</a>
						</div>
					</c:forEach>
				</div>
			</div>

			<div class="item-go">
				<a href="#">바로가기 →</a>
			</div>
		</div>


	</div>
</div>


