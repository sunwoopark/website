<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<link rel="stylesheet" href="/resources/css/include/content/choice.css" />

<div class="choice-wrap">
	<div class="choice-title">
		<img src="resources/images/home/choice/main_title01.jpg" alt="" width="1200px" height="100px">
	</div>
	<div class="choice-box">
		<ul class="box1-list flex-row-around">
			<li><a href="#"> <img src="resources/images/home/choice/choice_01.jpg" width="140px" height="140px">
					<p>사랑 고백</p>
			</a></li>
			<li><a href="#"> <img src="resources/images/home/choice/choice_02.jpg" width="140px" height="140px">
					<p>생일/결혼/기념일</p>
			</a></li>
			<li><a href="#"> <img src="resources/images/home/choice/choice_03.jpg" width="140px" height="140px">
					<p>승진/취업/영전</p>
			</a></li>
			<li><a href="#"> <img src="resources/images/home/choice/choice_04.jpg" width="140px" height="140px">
					<p>행사/공연/발표</p>
			</a></li>
			<li><a href="#"> <img src="resources/images/home/choice/choice_05.jpg" width="140px" height="140px">
					<p>출산/병문안</p>
			</a></li>
			<li><a href="#"> <img src="resources/images/home/choice/choice_06.jpg" width="140px" height="140px">
					<p>결혼/행사</p>
			</a></li>
			<li><a href="#"> <img src="resources/images/home/choice/choice_07.jpg" width="140px" height="140px">
					<p>조문/근조</p>
			</a></li>
		</ul>
	</div>
</div>