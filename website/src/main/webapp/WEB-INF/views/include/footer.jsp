<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<link rel="stylesheet" href="/resources/css/include/footer.css" />


<div class="footer">
	<div class="footer-wrap">
		<div class="footer-top">
			<ul class="flex-row-start">
				<li class="footer-menu01"><a href="#">&nbsp;회사소개</a><span>&nbsp;|</span></li>
				<li class="footer-menu01"><a href="#">이용약관</a><span>&nbsp;|</span></li>
				<li class="footer-menu01"><a href="#">개인정보취급방침</a><span>&nbsp;|</span></li>
				<li class="footer-menu01"><a href="#">고객센터</a><span>&nbsp;|</span></li>
				<li class="footer-menu01"><a href="${pageContext.request.contextPath}/mypage/qna1to1">1:1문의</a><span>&nbsp;|</span></li>
				<li class="footer-menu01"><a href="${pageContext.request.contextPath}/mypage/memberWithDraw">회원탈퇴신청</a></li>
			</ul>
		</div>
		<div class="footer-bottom">
			<div class="flex-row-between">
				<div class="footer-box01">
					<img src="http://placehold.it/250x300" alt="" />
				</div>
				<div class="footer-box02">
					<img src="http://placehold.it/580x300" alt="" />
				</div>
				<div class="footer-box03">
					<img src="http://placehold.it/310x300" alt="" />
				</div>
			</div>
		</div>
	</div>
	<div class="company-info-area">
		<div class="company-info">
			<strong>상호</strong> : (주)선우쇼핑몰 │ <strong>대표자</strong> : 박박박(help@test-flower.co.kr) <br /> <strong>사업자 등록번호</strong> : 123-12-12345 │ <strong>통신판매업</strong>
			제 2020-경기하남-0001호 <br /> <strong>주소</strong> : 경기도 하남시 망월동 872-3 │ <strong>TEL</strong> : 1500-1500 │ <strong>FAX</strong> : 02-1500-1500 <br />
			<strong>개인정보 보호책임자</strong> : 박박박 │ <strong>개인정보보유기간</strong> : 회원탈퇴시까지
		</div>
	</div>
</div>
