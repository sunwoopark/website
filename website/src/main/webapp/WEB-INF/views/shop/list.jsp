<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>


<html>
<head>
<title>박박박꽃배달</title>

<link rel="stylesheet" href="/resources/css/shop/list.css" />
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script>
	/* 	$(function() {
	 $("#btnWrite").click(function() {
	 location.href = "/admin/board/boardWrite";
	 });
	 });
	 */function list(page) {
		location.href = "/shop/list?c=${map.productCate1 }&curPage=" + page;
	}
</script>

</head>
<body>

	<div class="wrap">
		<!-- header 페이지 -->
		<%@ include file="../include/header.jsp"%>

		<!-- gnb 페이지 -->
		<%@ include file="../include/gnb.jsp"%>

		<section id="content">
			<div class="content-wrap">
				<ul>
					<c:forEach items="${map.list}" var="list">
						<li>
							<div>
								<a href="/shop/view?n=${list.productId}">
									<div class="pictureUrl">
										<img src="/resources/images/${list.pictureUrl}">
									</div>
									<div class="bar"></div>
									<div class="productInfo">
										<span>상품 명 : ${list.productName}</span>

										<p>상품 번호 : ${list.productId }</p>
										<p>
											상품 가격 :
											<fmt:formatNumber pattern="###,###,###" value="${list.price }" />
											원
										</p>
									</div>
								</a>
							</div>
						</li>
					</c:forEach>

					<div style="text-align: center; margin: 30px 0;">
						<c:if test="${map.pager.curBlock > 1 }">
							<a href="javascript:list('1')">[처음]</a>
						</c:if>
						<c:if test="${map.pager.curBlock > 1 }">
							<a href="javascript:list('${map.pager.prevPage }')">[이전]</a>
						</c:if>
						<c:forEach var="num" begin="${map.pager.blockBegin }" end="${map.pager.blockEnd }">
							<c:choose>
								<c:when test="${num == map.pager.curPage }">
									<span style="color: red;">${num }</span>&nbsp;
											</c:when>
								<c:otherwise>
									<a href="javascript:list('${num }')">${num }</a>&nbsp;
											</c:otherwise>
							</c:choose>
						</c:forEach>
						<c:if test="${map.pager.curBlock <= map.pager.totBlock}">
							<a href="javascript:list('${map.pager.nextPage }')">[다음]</a>
						</c:if>
						<c:if test="${map.pager.curPage <= map.pager.totPage}">
							<a href="javascript:list('${map.pager.totPage }')">[끝]</a>
						</c:if>
					</div>

				</ul>
			</div>

		</section>
		<!-- footer 페이지 -->
		<%@ include file="../include/footer.jsp"%>

	</div>


</body>
</html>
