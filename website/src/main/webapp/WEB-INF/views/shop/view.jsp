<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<link rel="stylesheet" href="/resources/css/shop/view.css" />


<html>
<head>
<title>박박박꽃배달</title>

<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="/resources/js/shop/view.js"></script>
<script>
	function cart_buy() {
		document.form1.action = "/order/buy";
		document.form1.submit();
	};

	function cart_insert() {
		document.form1.action = "/cart/insert";
		document.form1.submit();
	};
</script>

</head>
<body>

	<div class="wrap">
		<!-- header 페이지 -->
		<%@ include file="../include/header.jsp"%>

		<!-- gnb 페이지 -->
		<%@ include file="../include/gnb.jsp"%>


		<section id="content">
			<div class="boardView">
				<form name="form1" id="form1" method="post">
					<input type="hidden" name="productId" value="${dto.productId}">
					<div class="productInfo flex-row-between">
						<div class="productImg">
							<img src="/resources/images/${dto.pictureUrl}">
						</div>

						<div class="productContent">

							<table>
								<tr class="bNum">
									<td>상품코드</td>
									<td>${dto.productId}</td>
								</tr>
								<tr class="productName">
									<td>상품명</td>
									<td>${dto.productName}</td>
								</tr>
								<tr class="productCate1">
									<td>카테고리</td>
									<td><c:if test="${(dto.productCate1 == 1)}">꽃다발</c:if> <c:if test="${(dto.productCate1 == 2)}">꽃바구니</c:if> <c:if
											test="${(dto.productCate1 == 3)}">꽃상자</c:if> <c:if test="${(dto.productCate1 == 4)}">축하화환</c:if> <c:if test="${(dto.productCate1 == 5)}">근조화환</c:if>
										<c:if test="${(dto.productCate1 == 6)}">동양란</c:if> <c:if test="${(dto.productCate1 == 7)}">서양란</c:if> <c:if
											test="${(dto.productCate1 == 8)}">공기정화식물</c:if> <c:if test="${(dto.productCate1 == 9)}">기타</c:if></td>
								</tr>
								<tr class="price">
									<td>가격</td>
									<td><fmt:formatNumber pattern="###,###,###" value="${dto.price}" /> 원</td>
								</tr>

								<tr class="amount">
									<td>구입 수량</td>
									<td><select name="amount">
											<c:forEach begin="1" end="10" var="i">
												<option value="${i }">${i }</option>
											</c:forEach>
									</select>&nbsp; 개</td>
								</tr>
								<tr class="notice">
									<td colspan="2"><img src="/resources/images/shop/img-notice.jpg"></td>
								</tr>
								<tr>
									<td colspan="2" class="btnClass">
										<button type="button" class="buy_btn" onclick="cart_buy()">구매하기</button>
										<button type="button" class="cart_btn" onclick="cart_insert()">카트에 담기</button>
									</td>
								</tr>
							</table>
						</div>
					</div>
				</form>
				<div class="bar"></div>
				<div class="description">${dto.description}</div>
		</section>

		<!-- footer 페이지 -->
		<%@ include file="../include/footer.jsp"%>
	</div>


</body>
</html>
