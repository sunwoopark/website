<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<link rel="stylesheet" href="/resources/css/shop/cartList.css" />


<html>
<head>
<title>박박박꽃배달</title>

<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<!-- <script src="/resources/js/shop/cartList.js"></script> -->
<script src="/resources/js/include/go-first.js"></script>

<script>
	$(function() {
		$("#btnDelete").click(function() {
			if (confirm("장바구니를 비우시겠습니까?")) {
				location.href = "/cart/deleteAll";
			}
		});
	});
	$(function() {
		$('.order-btn').click(function() {
			if (confirm("주문페이지로 이동하시겠습니까?")) {
				location.href = "/order/insert";
			}
		});
	});
</script>
</head>
<body>

	<div class="wrap">
		<!-- header 페이지 -->
		<%@ include file="../include/header.jsp"%>

		<!-- gnb 페이지 -->
		<%@ include file="../include/gnb.jsp"%>


		<section id="content">

			<c:choose>
				<c:when test="${map.count == 0 }">
					<div class="mapCount">장바구니가 비었습니다.</div>
				</c:when>
				<c:otherwise>
					<form action="/cart/update" id="form1" name="form1" method="post">
						<table border="1" width="400px" class="cartList">
							<tr class="cartListName">
								<td>상품명</td>
								<td>이미지</td>
								<td>단가</td>
								<td>수량</td>
								<td>금액</td>
								<td>수정/삭제</td>
							</tr>
							<c:forEach var="row" items="${map.list }">
								<tr class="cartListInfo">
									<input type="hidden" value="${row.userId }" name="userId">
									<td>${row.productName }</td>
									<td><img src="/resources/images/${row.pictureUrl}"></td>
									<td><fmt:formatNumber value="${row.price }" pattern="#,###,###" /></td>
									<td><select name="amount">
											<c:forEach begin="1" end="10" var="i">
												<option value="${i }" <c:if test="${row.amount == i }">selected</c:if>>${i }</option>
											</c:forEach>
									</select>&nbsp; 개</td>
									<td><fmt:formatNumber value="${row.money }" pattern="#,###,###" /></td>
									<td><input type="hidden" name="cartId" value="${row.cartId}">
										<button type="submit" id="updateBtn" onclick="location.href='/cart/update?cartId=${row.cartId}&amount=${row.amount}'">수 정</button>
										<button type="button" id="deleteBtn" onclick="location.href='/cart/delete?cartId=${row.cartId}'">삭 제</button></td>
								</tr>
							</c:forEach>
						</table>
						<div class="cartTotal flex-row-between">
							<div>
								<button type="button" id="btnDelete">장바구니 비우기</button>
							</div>
							<div class="cartTotalMoney">
								<span> 장바구니 금액 합계 : <fmt:formatNumber value="${map.sumMoney }" pattern="#,###,###" />
								</span> <span> &nbsp;&nbsp;&nbsp;&nbsp;배송료 : <fmt:formatNumber value="${map.fee }" pattern="#,###,###" />
								</span> <span> &nbsp;&nbsp;&nbsp;&nbsp;총합계 : <fmt:formatNumber value="${map.sum }" pattern="#,###,###" />
								</span>
							</div>

						</div>
						<div class="btn">
							
							<button type="button" class="order-btn">주 문</button>
							<button type="button" class="cancel-btn go-first">처음으로</button>
						</div>
					</form>
				</c:otherwise>
			</c:choose>

		</section>

		<!-- footer 페이지 -->
		<%@ include file="../include/footer.jsp"%>
	</div>


</body>
</html>
