<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>



<html>
<head>
<title>박박박꽃배달</title>
<link rel="stylesheet" href="/resources/css/shop/order/orderView.css" />

<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="/resources/js/include/go-first.js"></script>


</head>
<body>

	<div class="wrap">
		<!-- header 페이지 -->
		<%@ include file="../../include/header.jsp"%>

		<!-- gnb 페이지 -->
		<%@ include file="../../include/gnb.jsp"%>

		<section id="content">
			<table class="orderAgreefrom">
				<colgroup>
					<col width="200px">
					<col width="200px">
					<col width="200px">
					<col width="200px">
					<col width="200px">
					<col width="200px">
				</colgroup>
				<tbody>
					<tr class="orderAgreeName">
						<td>주문번호</td>
						<td>상품명</td>
						<td>이미지</td>
						<td>결제금액</td>
						<td>결제일자</td>
						<td>배송상태</td>
					</tr>
					<c:forEach var="list" items="${list }">
						<tr class="orderAgreeInfo">
							<td>${list.orderNum }</td>
							<td>${list.productName }</td>
							<td><img src="/resources/images/${list.pictureUrl}"></td>
							<td><fmt:formatNumber value="${list.sumMoney }" pattern="#,###,###" /> 원</td>
							<td><fmt:formatDate value="${list.orderDate }" pattern="yyyy-MM-dd HH:mm:ss" /></td>
							<td>${list.delivery }</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>

			<p class="title1">주문자 정보</p>

			<form role="form" method="post" autocomplete="off">

				<table class="orderUserInfo">
					<colgroup>
						<col width="200px">
						<col width="900px">
					</colgroup>
					<tbody>
						<c:forEach var="list" items="${list }">
							<tr>
								<td>이름</td>
								<td>${list.orderUser }</td>
							</tr>
							<tr>
								<td>핸드폰번호</td>
								<td>${list.orderPhone }</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>


				<p class="title2">배송지 정보</p>

				<table class="orderAgreePost">
					<colgroup>
						<col width="200px">
						<col width="900px">
					</colgroup>
					<tbody>
						<c:forEach var="list" items="${list }">
							<tr>
								<td class="orderAgreeTitle">수령인</td>
								<td colspan="3" class="orderAgreeContent">${list.orderRec }</td>
							</tr>
							<tr>
								<td class="orderAgreeTitle">연락처</td>
								<td colspan="3" class="orderAgreeContent">${list.orderRecPhone}</td>
							</tr>
							<tr>
								<td class="orderAgreeTitle">주소지</td>
								<td class="orderAgreeContent">${list.orderAddr1}<br></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td class="orderAgreeContent">${list.orderAddr2}<br></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td class="orderAgreeContent">${list.orderAddr3}<br></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</form>
		</section>

		<!-- footer 페이지 -->
		<%@ include file="/WEB-INF\views\include\footer.jsp"%>
	</div>


</body>
</html>
