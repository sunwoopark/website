<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>



<html>
<head>
<title>박박박꽃배달</title>
<link rel="stylesheet" href="/resources/css/shop/order/orderList.css" />

<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="/resources/js/include/go-first.js"></script>


</head>
<body>

	<div class="wrap">
		<!-- header 페이지 -->
		<%@ include file="../../include/header.jsp"%>

		<!-- gnb 페이지 -->
		<%@ include file="../../include/gnb.jsp"%>

		<section id="content">
			<table class="orderlistfrom">
				<colgroup>
					<col width="200px">
					<col width="200px">
					<col width="200px">
					<col width="200px">
					<col width="200px">
					<col width="200px">
				</colgroup>
				<tbody>
					<tr class="orderListName">
						<td>주문번호</td>
						<td>상품명</td>
						<td>이미지</td>
						<td>결제일자</td>
						<td>결제금액</td>
						<td>배송상태</td>
					</tr>
					<c:forEach var="list" items="${list }">
						<tr class="orderListInfo">
							<td><a href="/order/orderView?orderNum=${list.orderNum }">${list.orderNum }</a></td>
							<td>${list.productName }</td>
							<td><img src="/resources/images/${list.pictureUrl}"></td>
							<td><fmt:formatDate value="${list.orderDate }" pattern="yyyy-MM-dd HH:mm:ss"/></td>
							<td><fmt:formatNumber value="${list.sumMoney }" pattern="#,###,###" /> 원</td>
							<td>${list.delivery }</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</section>

		<!-- footer 페이지 -->
		<%@ include file="/WEB-INF\views\include\footer.jsp"%>
	</div>


</body>
</html>
