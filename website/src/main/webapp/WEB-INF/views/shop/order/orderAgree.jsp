<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>



<html>
<head>
<title>박박박꽃배달</title>
<link rel="stylesheet" href="/resources/css/shop/order/orderAgree.css" />

<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="/resources/js/include/go-first.js"></script>


</head>
<body>

	<div class="wrap">
		<!-- header 페이지 -->
		<%@ include file="../../include/header.jsp"%>

		<!-- gnb 페이지 -->
		<%@ include file="../../include/gnb.jsp"%>

		<section id="content">
				<form action="/order/insertAgree" method="post" autocomplete="off">
			<table class="orderAgreefrom">
				<colgroup>
					<col width="200px">
					<col width="300px">
					<col width="300px">
					<col width="200px">
					<col width="200px">
				</colgroup>
				<tbody>
					<tr class="orderAgreeName">
						<td>상품명</td>
						<td>이미지</td>
						<td>단가</td>
						<td>수량</td>
						<td>금액</td>
					</tr>
					<c:forEach var="list" items="${map.list }">
						<input type="hidden" name="sumMoney" value="${map.sum }">	
						<tr class="orderAgreeInfo">
							<td>${list.productName }<input type="hidden" value="${list.productId }" name="productId"> </td>
							<td><img src="/resources/images/${list.pictureUrl}"></td>
							<td><fmt:formatNumber value="${list.price }" pattern="#,###,###" /></td>
							<td>${list.amount}개</td>
							<td><fmt:formatNumber value="${list.money }" pattern="#,###,###" /></td>
						</tr>
					</c:forEach>
					<tr>
						<td class="orderTotalMoney" colspan="5">
							<div>
								<fmt:formatNumber value="${map.sumMoney }" pattern="#,###,###" />
								원 / 배송료 :
								<fmt:formatNumber value="${map.fee }" pattern="#,###,###" />
								원 / 총합계 :
								<fmt:formatNumber value="${map.sum }" pattern="#,###,###"/>
								원
								
							</div>
						</td>
					</tr>
				</tbody>
			</table>

			<p class="title1">주문자 정보</p>

	
				
				<table class="orderUserInfo">
					<colgroup>
						<col width="200px">
						<col width="900px">
					</colgroup>
					<tbody>

						<tr>
							<td>이름</td>
							<td><input type="text" value="관리자" name="orderUser"></td>
						</tr>
						<tr>
							<td>핸드폰번호</td>
							<td><input type="text" name="orderPhone"></td>
						</tr>

					</tbody>
				</table>


				<p class="title2">배송지 정보</p>

				<table class="orderAgreePost">
					<colgroup>
						<col width="200px">
						<col width="900px">
					</colgroup>
					<tbody>
						<tr>
							<td class="orderAgreeTitle">수령인</td>
							<td colspan="3" class="orderAgreeContent"><input type="text" name="orderRec" id="orderRec" required="required"></td>
						</tr>
						<tr>
							<td class="orderAgreeTitle">연락처</td>
							<td colspan="3" class="orderAgreeContent"><input type="text" name="orderRecPhone" id="orderPhon" required="required"></td>
						</tr>
						<tr>
							<td class="orderAgreeTitle">우편번호</td>
							<td class="orderAgreeContent"><input type="text" id="sample6_postcode" placeholder="우편번호"><input type="button"
								onclick="sample6_execDaumPostcode()" value="우편번호 찾기" class="postbtn"></td>
						</tr>
						<tr>
							<td class="orderAgreeTitle">주소지</td>
							<td class="orderAgreeContent"><input type="text" name="orderAddr1" id="sample6_address" placeholder="주소"><br></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td class="orderAgreeContent"><input type="text" name="orderAddr2" id="sample6_detailAddress" placeholder="상세주소"></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td class="orderAgreeContent"><input type="text" name="orderAddr3" id="sample6_extraAddress" placeholder="참고항목"></td>
						</tr>

						<script src="https://t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js"></script>
						<script>
							function sample6_execDaumPostcode() {
								new daum.Postcode(
										{
											oncomplete : function(data) {
												// 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

												// 각 주소의 노출 규칙에 따라 주소를 조합한다.
												// 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
												var addr = ''; // 주소 변수
												var extraAddr = ''; // 참고항목 변수

												//사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
												if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
													addr = data.roadAddress;
												} else { // 사용자가 지번 주소를 선택했을 경우(J)
													addr = data.jibunAddress;
												}

												// 사용자가 선택한 주소가 도로명 타입일때 참고항목을 조합한다.
												if (data.userSelectedType === 'R') {
													// 법정동명이 있을 경우 추가한다. (법정리는 제외)
													// 법정동의 경우 마지막 문자가 "동/로/가"로 끝난다.
													if (data.bname !== ''
															&& /[동|로|가]$/g
																	.test(data.bname)) {
														extraAddr += data.bname;
													}
													// 건물명이 있고, 공동주택일 경우 추가한다.
													if (data.buildingName !== ''
															&& data.apartment === 'Y') {
														extraAddr += (extraAddr !== '' ? ', '
																+ data.buildingName
																: data.buildingName);
													}
													// 표시할 참고항목이 있을 경우, 괄호까지 추가한 최종 문자열을 만든다.
													if (extraAddr !== '') {
														extraAddr = ' ('
																+ extraAddr
																+ ')';
													}
													// 조합된 참고항목을 해당 필드에 넣는다.
													document
															.getElementById("sample6_extraAddress").value = extraAddr;

												} else {
													document
															.getElementById("sample6_extraAddress").value = '';
												}

												// 우편번호와 주소 정보를 해당 필드에 넣는다.
												document
														.getElementById('sample6_postcode').value = data.zonecode;
												document
														.getElementById("sample6_address").value = addr;
												// 커서를 상세주소 필드로 이동한다.
												document
														.getElementById(
																"sample6_detailAddress")
														.focus();
											}
										}).open();
							}
						</script>
					</tbody>
				</table>
				<div class="btnClass">
					
					<button type="submit" class="order_btn">주문</button>
					<button type="button" class="cancel_btn">취소</button>
				</div>
			</form>
		</section>

		<!-- footer 페이지 -->
		<%@ include file="/WEB-INF\views\include\footer.jsp"%>
	</div>


</body>
</html>
