<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<link rel="stylesheet" href="/resources/css/member/login.css" />

<html>
<head>
<title>박박박꽃배달</title>
</head>
<body>

	<div class="wrap">
		<!-- header 페이지 -->
		<%@ include file="../include/header.jsp"%>

		<!-- gnb 페이지 -->
		<%@ include file="../include/gnb.jsp"%>

		<div class="login-wrap">
			<div class="login-background">
				<div class="login-title">
					<ul class="flex-row-between">
						<li>LOGIN</li>
						<li><img src="http://placehold.it/300x30" alt=""></li>
					</ul>
				</div>
				<div class="login-detail flex-row-around">
					<div class="login-page">
						<form action="/member/loginResult" method="post" class="flex-col-between" autocomplete="off">
							<p>회원 로그인</p>
							<h1>가입하신 아이디와 비밀번호를 입력해주세요.</h1>
							<h1>아이디와 비밀번호는 대소문자를 구문합니다.</h1>
							<div class="login-box">
								<input type="text" class="user-id" name="userId" placeholder="아이디">
								<input type="password" class="user-password" name="userPass" placeholder="비밀번호">
								<button type="submit" class="login-btn">로그인 ></button>

								<br>
								<div class="login-box-etc">
									<ul class="flex-row-between">
										<li><input type="checkbox" class="login-checkbox">아이디 저장</li>
										<li><a href="#">아이디/비밀번호 찾기></a></li>
									</ul>
								</div>
							</div>
						</form>
					</div>
					<div class="login-center-bar"></div>
					<div class="join-page">
						<div class="join-box">
							<div class="join-title">
								<p>회원 가입</p>
								<h1>회원가입을 하시면 여러가지 혜택이 제공됩니다.</h1>
							</div>
							<div class="join-box-info">
								<ul>
									<li><a href="join">회원가입 ></a></li>
									<li style="height: 160px; margin-top: 50px;">
										<p>* 회원으로 가입하시면 구매하시는 상품의</p>
										<p>3%할인 or 7%적립 혜택을 받아보실 수 있습니다.</p>
										<p>* 비회원으로 구매하실 경우, 쿠폰 사용 및</p>
										<p>적립금 등의 혜택은 받으실 수 없습니다.</p>
										<p>* 회원가입 후 각종 이벤트에 대한 안내를</p>
										<p>받으시면 다양한 혜택을 받으실 수 있습니다.</p>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


		<!-- footer 페이지 -->
		<%@ include file="../include/footer.jsp"%>
	</div>


</body>
</html>
