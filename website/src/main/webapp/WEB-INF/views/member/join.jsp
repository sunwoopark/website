<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>박박박꽃배달</title>
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="/resources/css/member/join.css" />
<script src="/resources/js/member/boxCheck.js"></script>
<script src="/resources/js/include/go-first.js"></script>
</head>
<body>

	<div class="wrap">
		<!-- header 페이지 -->
		<%@ include file="../include/header.jsp"%>

		<!-- gnb 페이지 -->
		<%@ include file="../include/gnb.jsp"%>

		<div class="join-wrap">
			<div class="join-title-box">
				<h1>회원 가입 혜택</h1>
				<div class="join-title-bar"></div>
				<div class="join-title-bar"></div>
			</div>
			<div class="join-title-img">
				<ul class="flex-row-around">
					<li><a href=""> <img src="http://placehold.it/200x100" alt="">
					</a></li>
					<li><a href=""> <img src="http://placehold.it/200x100" alt="">
					</a></li>
					<li><a href=""> <img src="http://placehold.it/200x100" alt="">
					</a></li>
					<li><a href=""> <img src="http://placehold.it/200x100" alt="">
					</a></li>
				</ul>
			</div>
			<div class="join-agree">
				<form action="/member/joinAgree" name="userchkbox" method="post">
					<div class="join-agree-title">
						<h1>회원가입 약관 동의</h1>
						<p>※ 회원가입 전 반드시 읽어보시고, 약관과 고객약관, 개인정보취급방침에 동의하시면 '동의함' 버튼을 클릭해 주십시오.</p>
					</div>
					<div class="join-agree-content">
						<ul>
							<li>
								<h1>약관동의</h1> <textarea name="" id="" cols="168" rows="5">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</textarea>
								<label><input type="checkbox" name="chkagree">약관동의</label>
								<div class="join-agree-bar"></div>
							</li>
							<li>
								<h1>약관동의</h1> <textarea name="" id="" cols="168" rows="5">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</textarea>
								<label><input type="checkbox" name="chkagree">약관동의</label>
								<div class="join-agree-bar"></div>
							</li>
							<li>
								<h1>약관동의</h1> <textarea name="" id="" cols="168" rows="5">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</textarea> <label><input type="checkbox" name="chkagree">약관동의</label>
								<div class="join-agree-bar"></div>
							</li>
							<li><label> <input type="checkbox" class="check-all"><strong>[전체동의]</strong> 해당 약관에 모두 동의합니다. <br></label></li>
						</ul>
						<div class="btn">
							<input type="submit" value="가 입" class="join-agree-btn check-boxall"> 
							<input type="button" value="취 소" class="join-cancel-btn go-first">
						</div>

					</div>
				</form>

			</div>
		</div>


		<!-- footer 페이지 -->
		<%@ include file="../include/footer.jsp"%>
	</div>


</body>
</html>
