<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<html>
<head>
<title>박박박꽃배달</title>
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="/resources/js/member/idCheck.js"></script>
<script src="/resources/js/member/joinCheck.js"></script>
<script src="/resources/js/include/go-first.js"></script>
<link rel="stylesheet" href="/resources/css/member/join-agree.css" />

</head>
<body>

	<div class="wrap">
		<!-- header 페이지 -->
		<%@ include file="../include/header.jsp"%>

		<!-- gnb 페이지 -->
		<%@ include file="../include/gnb.jsp"%>

		<div class="join-agree-wrap">
			<div class="join-title-box">
				<h1>회원 가입 혜택</h1>
				<div class="join-title-bar"></div>
				<div class="join-title-bar"></div>
			</div>
			<div>
				<ul class="flex-row-around">
					<li><a> <img src="http://placehold.it/200x100" alt="">
					</a></li>
					<li><a> <img src="http://placehold.it/200x100" alt="">
					</a></li>
					<li><a> <img src="http://placehold.it/200x100" alt="">
					</a></li>
					<li><a> <img src="http://placehold.it/200x100" alt="">
					</a></li>
				</ul>
			</div>

			<div class="join-agree-final">
				<div class="join-agree-final-title">
					<p>회 원 가 입</p>
				</div>
				<div>
					<form method="post" action="/member/joinAgreeResult" onsubmit="return checkValue()"> 
					
						<table>
							<colgroup>
								<col width="180">
								<col width="20px">
								<col width="180px">
								<col width="180px">
								<col width="180px">
								<col width="180px">
								<col width="180px">
								<col width="180px">
							</colgroup>
							<tbody>
								<!-- 								<tr class="join-agree-final-box" style="border-top: 2px solid rgb(158, 158, 158);">
									<td class="agree-background">회원가입형태★</td>
									<td>&nbsp;</td>
									<td colspan="10"><label><input type="radio" name="userType" value="nomal" checked>&nbsp;개인회원가입</label> <label><input
											type="radio" name="userType" value="company">&nbsp;법인회원가입</label></td>
								</tr> -->
								<tr class="join-agree-final-box" style="border-top: 2px solid rgb(158, 158, 158);">
									<td class="agree-background">아이디★</td>
									<td>&nbsp;</td>
									<td colspan="10">
									<input type="text" id="userId" name="userId" maxlength="20">
									<input type="hidden" name="reId" value="">
									<input type="button" value="ID중복확인>" class="call-btn id-check"> 
									<span>&nbsp;&nbsp;&nbsp;&nbsp;[영문/숫자 조합 4~15자리]</span></td>
								</tr>
								<tr class="join-agree-final-box">
									<td class="agree-background">비밀번호★</td>
									<td>&nbsp;</td>
									<td><input type="password" size="20" id="userPass" name="userPass" maxlength="20"> <span>[4자리 이상]</span></td>
									<td class="agree-background">비밀번호 재입력★</td>
									<td>&nbsp;&nbsp; <input type="password" id="userPassChk" size="20" maxlength="20" name="userPassChk"> <span>[4자리 이상]</span>
									</td>
								</tr>
								<tr class="join-agree-final-box">
									<td class="agree-background">성명★</td>
									<td>&nbsp;</td>
									<td colspan="10"><input type="text" id="userName" name="userName" size="20" maxlength="20"> <span>[실명으로 기입]</span></td>
								</tr>
								<tr class="join-agree-final-box">
									<td class="agree-background">성별★</td>
									<td>&nbsp;</td>
									<td colspan="10"><label>
									<input type="radio" class="userGender" name="userGender" value="man">&nbsp;<strong>남자</strong></label> <label>
									<input type="radio" class="userGender" name="userGender" value="woman">&nbsp;<strong>여자</strong></label></td>
								</tr>
								<tr class="join-agree-final-box">
									<td class="agree-background">생년월일★</td>
									<td>&nbsp;</td>
									<td colspan="10"><select id="birthYY" name="birthYY" style="width: 90px;">
											<option value="" selected="">----</option>
											<option value="1970">1970</option>
											<option value="1971">1971</option>
											<option value="1972">1972</option>
											<option value="1973">1973</option>
											<option value="1974">1974</option>
											<option value="1975">1975</option>
											<option value="1976">1976</option>
											<option value="1977">1977</option>
											<option value="1978">1978</option>
											<option value="1979">1979</option>
											<option value="1980">1980</option>
											<option value="1981">1981</option>
											<option value="1982">1982</option>
											<option value="1983">1983</option>
											<option value="1984">1984</option>
											<option value="1985">1985</option>
											<option value="1986">1986</option>
											<option value="1987">1987</option>
											<option value="1988">1988</option>
											<option value="1989">1989</option>
											<option value="1990">1990</option>
											<option value="1991">1991</option>
											<option value="1992">1992</option>
											<option value="1993">1993</option>
											<option value="1994">1994</option>
											<option value="1995">1995</option>
											<option value="1996">1996</option>
											<option value="1997">1997</option>
											<option value="1998">1998</option>
											<option value="1999">1999</option>
											<option value="2000">2000</option>
											<option value="2001">2001</option>
											<option value="2002">2002</option>
											<option value="2003">2003</option>
											<option value="2004">2004</option>
											<option value="2005">2005</option>
											<option value="2006">2006</option>
											<option value="2007">2007</option>
											<option value="2008">2008</option>
											<option value="2009">2009</option>
											<option value="2010">2010</option>
									</select> <strong>년</strong> <select id="birthMM" name="birthMM" style="width: 70px;">
											<option value="" selected="">--</option>
											<option value="01">01</option>
											<option value="02">02</option>
											<option value="03">03</option>
											<option value="04">04</option>
											<option value="05">05</option>
											<option value="06">06</option>
											<option value="07">07</option>
											<option value="08">08</option>
											<option value="09">09</option>
											<option value="10">10</option>
											<option value="11">11</option>
											<option value="12">12</option>
									</select> <strong>월</strong> <select id="birthDD" name="birthDD" style="width: 90px;">
											<option value="" selected="">--</option>
											<option value="01">01</option>
											<option value="02">02</option>
											<option value="03">03</option>
											<option value="04">04</option>
											<option value="05">05</option>
											<option value="06">06</option>
											<option value="07">07</option>
											<option value="08">08</option>
											<option value="09">09</option>
											<option value="10">10</option>
											<option value="11">11</option>
											<option value="12">12</option>
											<option value="13">13</option>
											<option value="14">14</option>
											<option value="15">15</option>
											<option value="16">16</option>
											<option value="17">17</option>
											<option value="18">18</option>
											<option value="19">19</option>
											<option value="20">20</option>
											<option value="21">21</option>
											<option value="22">22</option>
											<option value="23">23</option>
											<option value="24">24</option>
											<option value="25">25</option>
											<option value="26">26</option>
											<option value="27">27</option>
											<option value="28">28</option>
											<option value="29">29</option>
											<option value="30">30</option>
											<option value="31">31</option>
									</select> <strong>일</strong></td>
								</tr>
								<tr class="join-agree-final-box">
									<td class="agree-background">핸드폰 번호★</td>
									<td>&nbsp;</td>
									<td colspan="10"><select id="phoneFF" name="phoneFF" style="width: 90px;">
											<option value="" selected="">-----</option>
											<option value="010">010</option>
											<option value="011">011</option>
											<option value="016">016</option>
											<option value="017">017</option>
											<option value="018">018</option>
											<option value="019">019</option>
									</select> - <input type="text" id="phoneSS" name="phoneSS" size="4" maxlength="4"> - <input type="text" id="phoneTT" name="phoneTT" size="4" maxlength="4"> <!--<a
										href="#" class="call-btn">  <span>인증번호 받기></span>
									</a>
										<div style="float: right; margin-right: 250px;">
											<input type="text" size="6"> <a href="#" class="call-btn"> <span>인증번호확인></span>
											</a>
										</div> --></td>
								</tr>
								<tr class="join-agree-final-box" style="border-bottom: 2px solid rgb(158, 158, 158);">
									<td class="agree-background">이메일★</td>
									<td>&nbsp;</td>
									<td colspan="10"><input type="email" size="33" id="userEmail" name="userEmail" maxlength="50"> <span>[본인의 정확한 E-mail]</span></td>
								</tr>
							</tbody>
							<div class="clear-both"></div>
						</table>

						<div style="text-align: center;">
							<input type="submit" value="가 입" class="join-agree-btn" >
							<input type="button" value="취 소" class="join-cancel-btn go-first">
						</div>

					</form>
				</div>

			</div>
		</div>
		<!-- footer 페이지 -->
		<%@ include file="../include/footer.jsp"%>
	</div>


</body>
</html>

