<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<html>
<head>
<title>Home</title>
</head>
<body>

	<div class="wrap">
		<!-- hearder 페이지 -->
		<%@include file="include/header.jsp"%>

		<!-- gnb 페이지 -->
		<%@ include file="include/gnb.jsp"%>

		<!-- content -->
		<div class="rolling">
			<%@ include file="include/content/rolling.jsp"%>
		</div>
		<div class="content">
			<%@ include file="include/content/choice.jsp"%>
			<%@ include file="include/content/best.jsp"%>
			<%@ include file="include/content/category.jsp"%>		
		</div>

		<!-- footer 페이지 -->
		<%@ include file="include/footer.jsp"%>

	</div>

</body>
</html>
