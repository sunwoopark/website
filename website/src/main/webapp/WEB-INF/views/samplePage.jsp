<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<html>
<head>
<title>Home</title>
</head>
<body>

	<div class="wrap">
		<!-- hearder 페이지 -->
		<%@include file="include/header.jsp"%>

		<!-- gnb 페이지 -->
		<%@ include file="include/gnb.jsp"%>

		<!-- content 페이지 -->

		<!-- footer 페이지 -->
		<%@ include file="include/footer.jsp"%>

	</div>

</body>
</html>
