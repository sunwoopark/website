$(function() {
    var goFirst = document.querySelector(".go-first");

    goFirst.onclick = function() {
        if (confirm("메인페이지로 이동하시겠습니까?")) {
            location.href = "/";
        }
    }
});