$(function() {
    var container = $('.slideshow'),
        slideGroup = container.find('.slidesshow_slides'),
        slides = slideGroup.find('a'),
        nav = container.find('.slidershow_nav'),
        indicator = container.find('.indicator'),
        slideCount = slides.length,
        indicatorHtml = '',
        currentIndex = 0,
        duration = 500,
        easing = 'swing',
        interval = 2500,
        timer;



    // 슬라이드를 가로로 배열
    slides.each(function(i) {
        var newLeft = i * 100 + '%';
        $(this).css({ left: newLeft });
        indicatorHtml += '<a href = "">' + (i + 1) + '</a>';

    });

    indicator.html(indicatorHtml);

    // 슬라이드 이동 함수
    function goToSlide(index) {
        slideGroup.animate({ left: -100 * index + '%' }, duration, easing);
        currentIndex = index;

        updateNav();


    };

    function updateNav() {
        var navPrev = nav.find('.prev');
        var navNext = nav.find('.next');

        if (currentIndex == 0) {
            navPrev.addClass('disabled');
        } else {
            navPrev.removeClass('disabled');
        };

        if (currentIndex == (slideCount - 1)) {
            navNext.addClass('disabled');
        } else {
            navNext.removeClass('disabled');
        };

        indicator.find('a').removeClass('active');
        indicator.find('a').eq(currentIndex).addClass('active');

    };


    // 인디케이터로 이동하기
    indicator.find('a').click(function(e) {
        e.preventDefault();
        var idx = $(this).index();
        goToSlide(idx);

    });

    // 좌우 버튼으로 이동하기
    // nav.find('.prev').click(function(e) {
    //     e.preventDefault();
    //     var i = currentIndex - 1;
    //     goToSlide(i);
    // })

    nav.find('a').click(function(e) {
        e.preventDefault();
        if ($(this).hasClass('prev')) {
            goToSlide(currentIndex - 1);
        } else {
            goToSlide(currentIndex + 1);
        }
    });

    updateNav();

    // 자동 슬라이드 함수
    function startTimer() {
        timer = setInterval(function() {
            var nextIndex = (currentIndex + 1) % slideCount;
            goToSlide(nextIndex);
        }, interval);
    }
    startTimer();

    function stopTimer() {
        clearInterval(timer)
    };

    container.mouseenter(function() {
        stopTimer();
    }).mouseleave(function() {
        startTimer();
    });
	
});


