$(function() {
    var goBack = document.querySelector(".go-back");

    goBack.onclick = function() {
        history.go(-1)();
    }
});