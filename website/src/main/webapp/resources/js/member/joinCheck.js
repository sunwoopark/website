function checkValue() {

  var userIdCheck = RegExp(/^[A-Za-z0-9_\-]{5,20}$/);

    if ($("#userId").val() == "") {
	    alert("아이디를 확인해 주세요.");
	    $("#userId").focus();
	    return false;
	}

      if ($("#userId").val().length < 4 || $("#userId").val().length > 15) {
          alert("아이디는 4~15자 이내여야 합니다.");
                $("#userId").focus();
          return false;
      }
  
      if ($("#userId").val().indexOf(" ") >= 0) {
          alert("아이디에는 공백이 들어가면 안됩니다.");
          $("#userId").focus();
          return false;
      }
      

    if ($("#userPass").val() == "") {
        alert("비밀번호를 확인해 주세요.");
        $("#userPass").focus();
        return false;
    }
    if ($("#userPassChk").val() == "") {
        alert("비밀번호를 확인해 주세요.");
        $("#userPassChk").focus();
        return false;
    }
    if ($("#userPass").val() != $("#userPassChk").val()) {
        alert("비밀번호가 일치하지 않습니다.");
        $("#userPass").focus();
        return false;
    }
    if ($("#userName").val() == "" || $("#userName").val().indexOf(" ") >= 0) {
        alert("이름을 입력해주세요.");
        $("#userName").focus();
        return false;
    }

    if ($("input:radio[name='userGender']:checked").length != 1) {
        alert("성별을 확인해 주세요.");
        return false;
    }

    if ($("#birthYY").val() == "" || $("#birthMM").val() == "" || $("#birthDD").val() == "") {
        alert("생년월일을 확인해주세요.");
        $("#birthYY").focus();
        return false;
    }

    if ($("#phoneFF").val() == "" || $("#phoneFF").val().indexOf(" ") >= 0 || $("#phoneSS").val() == "" || $("#phoneSS").val().indexOf(" ") >= 0 || $("#phoneTT").val() == "" || $("#phoneTT").val().indexOf(" ") >= 0) {
        alert("연락처를 확인해주세요.");
        $("#phoneFF").focus();
        return false;
    }


    if ($("#userEmail").val() == "") {
        alert("이메일을 입력해주세요.");
        $("#userEmail").focus();
        return false;
    }



    return true;
};