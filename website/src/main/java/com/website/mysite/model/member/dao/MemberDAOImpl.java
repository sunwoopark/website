package com.website.mysite.model.member.dao;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.website.mysite.model.member.dto.MemberDTO;

@Repository
public class MemberDAOImpl implements MemberDAO {

	@Inject
	SqlSession sqlsession;

	@Override
	public void joinAgree(MemberDTO dto) {
		sqlsession.insert("member.join_agree", dto);
	}

	@Override
	public int idCheck(MemberDTO dto) {
		return sqlsession.selectOne("member.idCheck", dto);
	}

	// 로그인
	@Override
	public MemberDTO login(MemberDTO dto) {
		return sqlsession.selectOne("member.login", dto);
	}

}
