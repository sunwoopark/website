package com.website.mysite.model.admin.dao;

import java.util.List;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.website.mysite.model.member.dto.MemberDTO;
import com.website.mysite.model.shop.dto.ProductDTO;

@Repository
public class AdminDAOImpl implements AdminDAO {

	@Inject
	SqlSession sqlSession;

	@Override
	public List<MemberDTO> userList() {
		return sqlSession.selectList("admin.user_list");
	}

	@Override
	public MemberDTO userUpdate(int userNum) {
		return sqlSession.selectOne("admin.userUpdate", userNum);
	}

	// 상품등록
	@Override
	public void productRegister(ProductDTO dto) {
		sqlSession.insert("product.product_register", dto);
	}

	// 상품 목록
	@Override
	public List<ProductDTO> productList() {
		return sqlSession.selectList("product.product_list");
	}

	@Override
	public ProductDTO productDetail(int productId) {
		return sqlSession.selectOne("product.product_detail", productId);
	}

	@Override
	public void productUpdate(ProductDTO dto) {
		sqlSession.update("product.product_update", dto);

	}

	@Override
	public void productDelete(int productId) {
		sqlSession.delete("product.product_delete", productId);

	}

	@Override
	public String fileInfo(int productId) {
		return sqlSession.selectOne("product.file_info", productId);
	}

}
