package com.website.mysite.model.shop.dao;

import java.util.List;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.website.mysite.model.shop.dto.CartDTO;
import com.website.mysite.model.shop.dto.CartListDTO;

@Repository
public class CartDAOImpl implements CartDAO {

	@Inject
	SqlSession sqlSession;

	@Override
	public void insert(CartDTO dto) {
		sqlSession.insert("cart.insert", dto);
	}

	@Override
	public List<CartListDTO> listCart(String userId) {
		return sqlSession.selectList("cart.listCart", userId);
	}

	@Override
	public int sumMoney(String userId) {
		return sqlSession.selectOne("cart.sumMoney", userId);
	}

	@Override
	public List<CartDTO> cartMoney() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(int cartId) {
		sqlSession.delete("cart.delete", cartId);
	}

	@Override
	public void deleteAll(String userId) {
		sqlSession.delete("cart.deleteAll", userId);

	}

	@Override
	public void modifyCart(CartDTO dto) {
		sqlSession.update("cart.modify", dto);
	}

	@Override
	public void update(int cartId) {
		// TODO Auto-generated method stub

	}

	@Override
	public int countCart(String userid, int productId) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void updateCart(CartDTO dto) {
		// TODO Auto-generated method stub

	}

}
