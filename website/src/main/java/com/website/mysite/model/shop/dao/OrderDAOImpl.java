package com.website.mysite.model.shop.dao;

import java.util.List;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.website.mysite.model.shop.dto.CartListDTO;
import com.website.mysite.model.shop.dto.OrderDTO;

@Repository
public class OrderDAOImpl implements OrderDAO {

	@Inject
	SqlSession sqlSession;

	// 상품구매
	@Override
	public void orderInsert(OrderDTO dto) {
		sqlSession.insert("order.orderInsert", dto);
	}

	@Override
	public List<OrderDTO> orderList() {
		return sqlSession.selectList("admin.orderList");
	}

	@Override
	public List<CartListDTO> orderList(String userId) {
		return sqlSession.selectList("order.orderList", userId);
	}

	@Override
	public List<OrderDTO> orderView(String orderNum) {
		return sqlSession.selectList("order.orderView", orderNum);
	}

	@Override
	public void delivery(OrderDTO dto) {
		sqlSession.update("admin.delivery", dto);

	}

}
