package com.website.mysite.model.shop.dto;

import org.springframework.web.multipart.MultipartFile;

public class ProductDTO {
	private int productId;
	private String productName;
	private int price;
	private String description;
	private String pictureUrl;
	private MultipartFile file1;

	private int productCate1;
	private int productCate2;

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPictureUrl() {
		return pictureUrl;
	}

	public void setPictureUrl(String pictureUrl) {
		this.pictureUrl = pictureUrl;
	}

	public MultipartFile getFile1() {
		return file1;
	}

	public void setFile1(MultipartFile file1) {
		this.file1 = file1;
	}

	public int getProductCate1() {
		return productCate1;
	}

	public void setProductCate1(int productCate1) {
		this.productCate1 = productCate1;
	}

	public int getProductCate2() {
		return productCate2;
	}

	public void setProductCate2(int productCate2) {
		this.productCate2 = productCate2;
	}

}
