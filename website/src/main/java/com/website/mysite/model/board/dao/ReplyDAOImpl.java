package com.website.mysite.model.board.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.website.mysite.model.board.dto.ReplyDTO;

@Repository
public class ReplyDAOImpl implements ReplyDAO {

	@Inject
	SqlSession sqlSession;

	@Override
	public void create(ReplyDTO dto) {
		sqlSession.insert("reply.insertReply", dto);
	}

	@Override
	public List<ReplyDTO> list(Integer bNum, int start, int end) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("start", start);
		map.put("end", end);
		map.put("bNum", bNum);
		return sqlSession.selectList("reply.listReply", map);
	}

	@Override
	public int count(int bNum) {
		return sqlSession.selectOne("reply.count", bNum);
	}

	@Override
	public void update(ReplyDTO dto) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(Integer rNum) {
		// TODO Auto-generated method stub

	}

	@Override
	public ReplyDTO detail(int rNum) {
		// TODO Auto-generated method stub
		return null;
	}

}
