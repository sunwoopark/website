package com.website.mysite.model.shop.dao;

import java.util.List;

import com.website.mysite.model.shop.dto.CartDTO;
import com.website.mysite.model.shop.dto.CartListDTO;

public interface CartDAO {

	public List<CartDTO> cartMoney();

	public void insert(CartDTO dto); // 장바구니 추가

	public List<CartListDTO> listCart(String userId); // 장바구니 목록

	public void delete(int cartId); // 장바구니 개별삭제

	public void deleteAll(String userId); // 장바구니 비우기

	public int sumMoney(String userId); // 장바구니 금액 합계

	public int countCart(String userId, int productId); // 장바구니 상품 갯수

	public void modifyCart(CartDTO dto);
	/**/

	public void update(int cartId);

	public void updateCart(CartDTO dto); // 장바구니 수정

}
