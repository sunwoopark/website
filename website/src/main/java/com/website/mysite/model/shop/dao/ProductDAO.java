package com.website.mysite.model.shop.dao;

import java.util.List;

import com.website.mysite.model.shop.dto.ProductDTO;

public interface ProductDAO {

	// 상품 리스트
	public List<ProductDTO> listProduct(int productCate1, int start, int end);

	public int countProduct(int productCate1);

	// 검색
	public List<ProductDTO> listSearch(int start, int end, String keyword);

	public int countSearch(String keyword);

	public ProductDTO detailProduct(int productId);

	// home
	public List<ProductDTO> productList(int productCate1, int productCate2);

}
