package com.website.mysite.model.board.dao;

import java.util.List;

import com.website.mysite.model.board.dto.BoardDTO;

public interface BoardDAO {
	public void deleteFile(String fullName);

	public List<String> getAttach(int bno); // 첨부파일 목록

	public void addAttach(String fullName); // 첨부파일 저장

	public void updateAttach(String fullName, int bno); // 첨부파일 수정

	public void write(BoardDTO dto) throws Exception; // 글쓰기

	public BoardDTO read(int bNum) throws Exception;

	public void increaseViewcnt(int bNum) throws Exception;

	public void boardUpdate(BoardDTO dto) throws Exception;

	public void boardDelete(int bNum) throws Exception;

	public void boardReplyDelete(int bNum) throws Exception;

	public List<BoardDTO> listAll(int start, int end, String search_option, String keyword) throws Exception;

	public int countArticle(String search_option, String keyword) throws Exception;
}
