package com.website.mysite.model.shop.dto;

import java.util.Date;

public class OrderDTO {

	private String orderNum;
	private String userId;
	private String orderUser;
	private String orderAddr1;
	private String orderAddr2;
	private String orderAddr3;
	private String orderPhone;
	private Date orderDate;
	private String delivery;
	private String orderRec;
	private String orderRecPhone;

	/**/
	private int productId;
	private String productName;
	private int price;
	private int amount;
	private int sumMoney;
	private String pictureUrl;

	public String getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getOrderUser() {
		return orderUser;
	}

	public void setOrderUser(String orderUser) {
		this.orderUser = orderUser;
	}

	public String getOrderAddr1() {
		return orderAddr1;
	}

	public void setOrderAddr1(String orderAddr1) {
		this.orderAddr1 = orderAddr1;
	}

	public String getOrderAddr2() {
		return orderAddr2;
	}

	public void setOrderAddr2(String orderAddr2) {
		this.orderAddr2 = orderAddr2;
	}

	public String getOrderAddr3() {
		return orderAddr3;
	}

	public void setOrderAddr3(String orderAddr3) {
		this.orderAddr3 = orderAddr3;
	}

	public String getOrderPhone() {
		return orderPhone;
	}

	public void setOrderPhone(String orderPhone) {
		this.orderPhone = orderPhone;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public String getDelivery() {
		return delivery;
	}

	public void setDelivery(String delivery) {
		this.delivery = delivery;
	}

	public String getOrderRec() {
		return orderRec;
	}

	public void setOrderRec(String orderRec) {
		this.orderRec = orderRec;
	}

	public String getOrderRecPhone() {
		return orderRecPhone;
	}

	public void setOrderRecPhone(String orderRecPhone) {
		this.orderRecPhone = orderRecPhone;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public int getSumMoney() {
		return sumMoney;
	}

	public void setSumMoney(int sumMoney) {
		this.sumMoney = sumMoney;
	}

	public String getPictureUrl() {
		return pictureUrl;
	}

	public void setPictureUrl(String pictureUrl) {
		this.pictureUrl = pictureUrl;
	}

}
