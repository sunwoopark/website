package com.website.mysite.model.board.dao;

import java.util.List;

import com.website.mysite.model.board.dto.ReplyDTO;

public interface ReplyDAO {

	public List<ReplyDTO> list(Integer bNum, int start, int end);

	public int count(int bNum);

	public void create(ReplyDTO dto);

	public void update(ReplyDTO dto);

	public void delete(Integer rNum);

	public ReplyDTO detail(int rNum);

}
