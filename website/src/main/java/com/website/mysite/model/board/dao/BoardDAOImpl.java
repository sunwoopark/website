package com.website.mysite.model.board.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.website.mysite.model.board.dto.BoardDTO;

@Repository
public class BoardDAOImpl implements BoardDAO {

	@Inject
	SqlSession sqlSession;

	@Override
	public List<BoardDTO> listAll(int start, int end, String search_option, String keyword) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("start", start);
		map.put("end", end);
		map.put("search_option", search_option);
		map.put("keyword", keyword);
		return sqlSession.selectList("mypage.listAll", map);
	}

	@Override
	public void write(BoardDTO dto) throws Exception {
		sqlSession.insert("mypage.write", dto);
	}

	@Override
	public int countArticle(String search_option, String keyword) throws Exception {
		Map<String, String> map = new HashMap<String, String>();
		map.put("search_option", search_option);
		map.put("keyword", keyword);
		return sqlSession.selectOne("mypage.countArticle", map);
	}

	@Override
	public BoardDTO read(int bNum) throws Exception {
		return sqlSession.selectOne("mypage.view", bNum);
	}

	@Override
	public void increaseViewcnt(int bNum) throws Exception {
		sqlSession.update("mypage.increaseViewcnt", bNum);

	}

	@Override
	public void deleteFile(String fullName) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<String> getAttach(int bno) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addAttach(String fullName) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateAttach(String fullName, int bno) {
		// TODO Auto-generated method stub

	}

	/**/
	@Override
	public void boardUpdate(BoardDTO dto) throws Exception {
		sqlSession.update("mypage.boardUpdate", dto);

	}

	@Override
	public void boardDelete(int bNum) throws Exception {
		sqlSession.delete("mypage.boardDelete", bNum);

	}

	@Override
	public void boardReplyDelete(int bNum) throws Exception {
		sqlSession.delete("mypage.boardReplyDelete", bNum);

	}

}
