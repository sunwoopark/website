package com.website.mysite.model.shop.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.website.mysite.model.shop.dto.ProductDTO;

@Repository
public class ProductDAOImpl implements ProductDAO {

	@Inject
	SqlSession sqlSession;

	// home
	@Override
	public List<ProductDTO> productList(int productCate1, int productCate2) {
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("productCate1", productCate1);
		map.put("productCate2", productCate2);
		return sqlSession.selectList("home.productList", map);
	}

	@Override
	public List<ProductDTO> listProduct(int productCate1, int start, int end) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("productCate1", productCate1);
		map.put("start", start);
		map.put("end", end);

		return sqlSession.selectList("product.list_product", map);
	}

	@Override
	public int countProduct(int productCate1) {
		return sqlSession.selectOne("product.count_product", productCate1);
	}

	// 검색
	@Override
	public List<ProductDTO> listSearch(int start, int end, String keyword) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("start", start);
		map.put("end", end);
		map.put("keyword", keyword);
		return sqlSession.selectList("product.list_search", map);
	}

	@Override
	public int countSearch(String keyword) {
		return sqlSession.selectOne("product.count_search", keyword);
	}

	@Override
	public ProductDTO detailProduct(int productId) {
		return sqlSession.selectOne("product.detail_product", productId);
	}

}
