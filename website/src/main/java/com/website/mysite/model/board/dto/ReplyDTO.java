package com.website.mysite.model.board.dto;

import java.util.Date;

public class ReplyDTO {

	private Integer rNum;
	private Integer bNum;
	private String replytext;
	private String replyer;
	private Date regdate;
	private Date updatedate;

	private String userId;
	private String userName;

	private String secret_reply;

	public Integer getrNum() {
		return rNum;
	}

	public void setrNum(Integer rNum) {
		this.rNum = rNum;
	}

	public Integer getbNum() {
		return bNum;
	}

	public void setbNum(Integer bNum) {
		this.bNum = bNum;
	}

	public String getReplytext() {
		return replytext;
	}

	public void setReplytext(String replytext) {
		this.replytext = replytext;
	}

	public String getReplyer() {
		return replyer;
	}

	public void setReplyer(String replyer) {
		this.replyer = replyer;
	}

	public Date getRegdate() {
		return regdate;
	}

	public void setRegdate(Date regdate) {
		this.regdate = regdate;
	}

	public Date getUpdatedate() {
		return updatedate;
	}

	public void setUpdatedate(Date updatedate) {
		this.updatedate = updatedate;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getSecret_reply() {
		return secret_reply;
	}

	public void setSecret_reply(String secret_reply) {
		this.secret_reply = secret_reply;
	}

}
