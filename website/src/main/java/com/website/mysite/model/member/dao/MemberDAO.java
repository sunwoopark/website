package com.website.mysite.model.member.dao;

import com.website.mysite.model.member.dto.MemberDTO;

public interface MemberDAO {

	// 회원가입
	public void joinAgree(MemberDTO dto);

	// 아이디 중복 체크
	public int idCheck(MemberDTO dto);

	// 로그인
	public MemberDTO login(MemberDTO dto);

	/*
	 * public List<MemberDTO> memberList();
	 * 
	 * public void insertMember(MemberDTO dto);
	 * 
	 * public MemberDTO viewMember(String userid);
	 * 
	 * public void deleteMember(String userid);
	 * 
	 * public void updateMember(MemberDTO dto);
	 */

}
