package com.website.mysite.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.website.mysite.model.shop.dto.ProductDTO;
import com.website.mysite.service.shop.ProductService;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {

	@Inject
	ProductService productService;

	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView home(ModelAndView mav) {
		logger.info("home page");

		Map<String, Object> map = new HashMap<String, Object>();

		for (int i = 1; i < 9; i++) {
			Map<String, List<ProductDTO>> Map = productService.listProduct(i);
			map.put("Map" + i, Map);
		}

		mav.setViewName("home");
		mav.addObject("m", map);
		return mav;
	}

}
