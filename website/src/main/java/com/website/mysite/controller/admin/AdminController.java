package com.website.mysite.controller.admin;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.website.mysite.model.board.dto.BoardDTO;
import com.website.mysite.model.member.dto.MemberDTO;
import com.website.mysite.model.shop.dto.OrderDTO;
import com.website.mysite.model.shop.dto.ProductDTO;
import com.website.mysite.service.admin.AdminService;
import com.website.mysite.service.board.BoardService;
import com.website.mysite.service.board.Pager;
import com.website.mysite.service.shop.OrderService;

@Controller
@RequestMapping("/admin/*")
public class AdminController {

	private static final Logger LOGGER = LoggerFactory.getLogger(AdminController.class);

	@Inject
	AdminService adminService;

	@Inject
	BoardService boardService;

	@Inject
	OrderService orderService;

	// 메인
	@RequestMapping("index")
	public String adminIndex() {
		LOGGER.info("admin page");
		return "admin/index";
	}

	// 상품 등록
	@RequestMapping("productRegister")
	public String productRegister() {
		return "admin/product/productRegister";
	}

	// 상품 등록 결과
	@RequestMapping("productRegisterResult")
	public String productRegisterResult(ProductDTO dto) {
		String filename = "-";
		if (!dto.getFile1().isEmpty()) { // 첨부파일이 있으면
			Date from = new Date();
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMddhhmmss");
			String format2 = format.format(from);

			filename = format2 + "_" + dto.getFile1().getOriginalFilename();
			String path = "D:\\website\\.metadata\\.plugins\\org.eclipse.wst.server.core\\tmp0\\wtpwebapps\\website\\resources\\images\\";

			try {
				new File(path).mkdir();
				dto.getFile1().transferTo(new File(path + filename));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		dto.setPictureUrl(filename);
		adminService.productRegister(dto);

		return "redirect:/admin/index";
	}

	// 상품 목록
	@RequestMapping("productList")
	public ModelAndView productList(ModelAndView mav) {
		mav.setViewName("admin/product/productList");
		mav.addObject("list", adminService.productList());
		return mav;

	}

	// 상품 상세
	@RequestMapping("productDetail")
	public ModelAndView detail(@RequestParam("n") int productId, ModelAndView mav) {
		mav.setViewName("admin/product/productDetail");
		mav.addObject("dto", adminService.productDetail(productId));

		return mav;
	}

	// 상품 수정
	@RequestMapping("productUpdate")
	public ModelAndView update(@RequestParam("productId") int productId, ModelAndView mav) {
		mav.setViewName("admin/product/productUpdate");
		mav.addObject("dto", adminService.productDetail(productId));
		return mav;
	}

	// 상품 수정 결과
	@RequestMapping("productUpdateResult")
	public String updateResult(ProductDTO dto) {
		System.out.println(dto.toString());
		String filename = "-";

		if (!dto.getFile1().isEmpty()) { // 첨부파일이 있으면
			Date from = new Date();
			SimpleDateFormat format = new SimpleDateFormat("yyyyMMddhhmmss");
			String format2 = format.format(from);
			filename = format2 + "_" + dto.getFile1().getOriginalFilename();

			String path = "D:\\website\\.metadata\\.plugins\\org.eclipse.wst.server.core\\tmp0\\wtpwebapps\\website\\resources\\images\\";
			try {
				new File(path).mkdir();
				dto.getFile1().transferTo(new File(path + filename));
			} catch (Exception e) {
				e.printStackTrace();
			}
			dto.setPictureUrl(filename);
		} else {
			ProductDTO dto2 = adminService.productDetail(dto.getProductId());
			dto.setPictureUrl(dto2.getPictureUrl());
		}

		adminService.productUpdate(dto);
		return "redirect:/admin/productList";
	}

	// 상품 삭제
	@RequestMapping("productDelete")
	public String delete(@RequestParam("productId") int productId) {
		// 첨부파일 이름

		String filename = adminService.fileInfo(productId);
		if (filename != null && !filename.equals("-")) {
			String path = "D:\\website\\.metadata\\.plugins\\org.eclipse.wst.server.core\\tmp0\\wtpwebapps\\website\\resources\\images\\";
			File f = new File(path + filename);
			if (f.exists()) { // 파일이 존재 하면
				f.exists(); // 파일삭제
			}
		}

		adminService.productDelete(productId);
		return "redirect:/admin/productList";
	}

	/***********************************************/

	// 유저 목록
	@RequestMapping("userList")
	public ModelAndView userList(ModelAndView mav) {
		List<MemberDTO> userInfo = adminService.userList();
		mav.setViewName("admin/user/userList");
		mav.addObject("userInfo", userInfo);
		return mav;
	}

	// 유저 정보 수정
	@RequestMapping("userUpdate")
	public ModelAndView userUpdate(ModelAndView mav, @RequestParam int userNum) {
		MemberDTO userInfo = adminService.userUpdate(userNum);
		mav.setViewName("admin/user/userUpdate");
		mav.addObject("userInfo", userInfo);
		return mav;
	}

	// 유저 정보 수정 결과
	@RequestMapping("userUpdateResult")
	public String userUpdateResult(@ModelAttribute BoardDTO dto) throws Exception {
//		boardService.boardUpdate(dto);

		return "redirect:/admin/userList";
	}

	/***********************************************/

	// 게시글 등록
	@RequestMapping("board/boardWrite")
	public String boardWrite() {
		return "admin/board/boardWrite";
	}

	// 게시글 등록 결과
	@RequestMapping("board/boardWriteResult")
	public String boardWriteResult(@ModelAttribute BoardDTO dto, HttpSession session) throws Exception {
		MemberDTO userId = (MemberDTO) session.getAttribute("userId");
		dto.setUserId(userId.getUserId());
		boardService.write(dto);

		return "redirect:/admin/board/boardList";
	}

	// 게시글 목록
	@RequestMapping("board/boardList")
	public ModelAndView boardList(@RequestParam(defaultValue = "1") int curPage, @RequestParam(defaultValue = "all") String search_option,
			@RequestParam(defaultValue = "") String keyword) throws Exception {
		int count = boardService.countArticle(search_option, keyword);
		Pager pager = new Pager(count, curPage);
		int start = pager.getPageBegin();
		int end = pager.getPageEnd();

		List<BoardDTO> list = boardService.listAll(start, end, search_option, keyword); // 목록
		ModelAndView mav = new ModelAndView();
		mav.setViewName("admin/board/boardList");
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("list", list);
		map.put("count", count);
		map.put("search_option", search_option);
		map.put("keyword", keyword);
		map.put("pager", pager);
		mav.addObject("map", map);
		return mav;

	}

	// 게시글 수정
	@RequestMapping("board/boardUpdate")
	public ModelAndView boardUpdate(@RequestParam("bNum") int bNum, ModelAndView mav) throws Exception {
		mav.setViewName("admin/board/boardUpdate");
		mav.addObject("dto", boardService.read(bNum));
		return mav;
	}

	// 게시글 수정 결과
	@RequestMapping("board/boardUpdateResult")
	public String boardUpdateResult(@ModelAttribute BoardDTO dto) throws Exception {
		boardService.boardUpdate(dto);

		return "redirect:/admin/board/boardList";
	}

	// 게시글 삭제
	@RequestMapping("board/boardDelete")
	public String boardDelete(@RequestParam("bNum") int bNum) throws Exception {
		boardService.boardReplyDelete(bNum);
		boardService.boardDelete(bNum);
		return "redirect:/admin/board/boardList";
	}

	// 게시물 상세
	@RequestMapping(value = "board/boardView", method = RequestMethod.GET)
	public ModelAndView view(@RequestParam int bNum, @RequestParam int curPage, @RequestParam String search_option, @RequestParam String keyword,
			HttpSession session) throws Exception {

		boardService.increaseViewcnt(bNum);
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/admin/board/boardView");
		mav.addObject("dto", boardService.read(bNum));
		mav.addObject("curPage", curPage);
		mav.addObject("search_option", search_option);
		mav.addObject("keyword", keyword);

		return mav;
	}

	/***********************************************/

	// 상품 구매 목록
	@RequestMapping("/orderList")
	public ModelAndView orderList(ModelAndView mav) throws Exception {

		mav.setViewName("admin/order/orderList");
		mav.addObject("list", orderService.orderList());
		return mav;

	}

	// 상품 구매 상세
	@RequestMapping(value = "/orderView", method = RequestMethod.GET)
	public ModelAndView orderView(ModelAndView mav, @RequestParam String orderNum) throws Exception {

		mav.setViewName("admin/order/orderView");
		mav.addObject("list", orderService.orderView(orderNum));
		return mav;
	}

	// 상품 배송 상태 변경
	@RequestMapping(value = "/orderView", method = RequestMethod.POST)
	public String delivery(OrderDTO dto) throws Exception {

		orderService.delivery(dto);

		return "redirect:/admin/orderView?orderNum=" + dto.getOrderNum();
	}

	/***********************************************/

}
