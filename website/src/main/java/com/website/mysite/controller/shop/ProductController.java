package com.website.mysite.controller.shop;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.website.mysite.service.board.Pager;
import com.website.mysite.service.shop.ProductService;

@Controller
@RequestMapping("/shop/*")
public class ProductController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProductController.class);

	@Inject
	ProductService productService;

	// 상품 리스트
	@RequestMapping("list")
	public ModelAndView list(ModelAndView mav, @RequestParam("c") int productCate1, @RequestParam(defaultValue = "1") int curPage) {
		Map<String, Object> map = new HashMap<String, Object>();

		int count = productService.countProduct(productCate1);
		Pager pager = new Pager(count, curPage);
		int start = pager.getPageBegin();
		int end = pager.getPageEnd();

		mav.setViewName("/shop/list");
		map.put("count", count);
		map.put("pager", pager);
		map.put("list", productService.listProduct(productCate1, start, end));
		map.put("productCate1", productCate1);
		mav.addObject("map", map);

		return mav;
	}

	// 상품 검색
	@RequestMapping("listSearch")
	public ModelAndView listSearch(ModelAndView mav, @RequestParam(defaultValue = "1") int curPage, @RequestParam(defaultValue = "") String keyword)
			throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();

		int count = productService.countSearch(keyword);
		Pager pager = new Pager(count, curPage);
		int start = pager.getPageBegin();
		int end = pager.getPageEnd();

		mav.setViewName("/shop/listSearch");
		map.put("count", count);
		map.put("pager", pager);
		map.put("list", productService.listSearch(start, end, keyword));
		mav.addObject("map", map);

		return mav;
	}

	// 상품 상세
	@RequestMapping("view")
	public ModelAndView view(@RequestParam("n") int product_id, ModelAndView mav) {
		mav.setViewName("/shop/view");
		mav.addObject("dto", productService.detailProduct(product_id));
		return mav;
	}

}
