package com.website.mysite.controller.shop;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.website.mysite.model.member.dto.MemberDTO;
import com.website.mysite.model.shop.dto.CartDTO;
import com.website.mysite.model.shop.dto.CartListDTO;
import com.website.mysite.model.shop.dto.OrderDTO;
import com.website.mysite.service.shop.CartService;
import com.website.mysite.service.shop.OrderService;

@Controller
@RequestMapping("/order/*")
public class OrderController {

	private static final Logger LOGGER = LoggerFactory.getLogger(OrderController.class);

	@Inject
	OrderService orderService;

	@Inject
	CartService cartService;

	@RequestMapping("insert")
	public ModelAndView insert(HttpSession session, CartDTO dto, ModelAndView mav) {
		MemberDTO userId = (MemberDTO) session.getAttribute("userId");

		Map<String, Object> map = new HashMap<String, Object>();
		List<CartListDTO> list = cartService.listCart(userId.getUserId());

		int sumMoney = cartService.sumMoney(userId.getUserId());
		int fee = sumMoney >= 30000 ? 0 : 2500;
		map.put("sumMoney", sumMoney);
		map.put("fee", fee);
		map.put("sum", sumMoney + fee);
		map.put("list", list);
		map.put("count", list.size());

		mav.setViewName("shop/order/orderAgree");
		mav.addObject("map", map);
		return mav;
	}

	@RequestMapping("insertAgree")
	public String insertAgree(HttpSession session, @ModelAttribute OrderDTO dto) throws Exception {
		MemberDTO userId = (MemberDTO) session.getAttribute("userId");
		dto.setUserId(userId.getUserId());

		Calendar cal = Calendar.getInstance();
		int year = cal.get(Calendar.YEAR);
		String ym = year + new DecimalFormat("00").format(cal.get(Calendar.MONTH) + 1);
		String ymd = ym + new DecimalFormat("00").format(cal.get(Calendar.DATE));
		String subNum = "";

		for (int i = 1; i <= 6; i++) {
			subNum += (int) (Math.random() * 10);
		}
		String orderNum = ymd + "_" + subNum;
		dto.setOrderNum(orderNum);

		orderService.orderInsert(dto);
		cartService.deleteAll(userId.getUserId());

		return "redirect:/order/orderList";

	}

	@RequestMapping("orderList")
	public ModelAndView orderList(HttpSession session, ModelAndView mav) {
		MemberDTO userId = (MemberDTO) session.getAttribute("userId");

		mav.setViewName("shop/order/orderList");
		mav.addObject("list", orderService.orderList(userId.getUserId()));
		return mav;
	}

	@RequestMapping("orderView")
	public ModelAndView orderView(HttpSession session, ModelAndView mav, @RequestParam String orderNum) {

		mav.setViewName("shop/order/orderView");
		mav.addObject("list", orderService.orderView(orderNum));

		return mav;
	}

}
