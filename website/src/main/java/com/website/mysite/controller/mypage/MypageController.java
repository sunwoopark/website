package com.website.mysite.controller.mypage;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.website.mysite.model.board.dto.BoardDTO;
import com.website.mysite.service.board.BoardService;
import com.website.mysite.service.board.Pager;

@Controller
@RequestMapping("/mypage/*")
public class MypageController {

	private static final Logger LOGGER = LoggerFactory.getLogger(MypageController.class);

	@Inject
	BoardService boardService;

	// 메인페이지
	@RequestMapping("index")
	public String index() {
		return "mypage/index";
	}

	// 공지사항
	@RequestMapping("boardList")
	public ModelAndView boardList(@RequestParam(defaultValue = "1") int curPage, @RequestParam(defaultValue = "all") String search_option,
			@RequestParam(defaultValue = "") String keyword) throws Exception {
		int count = boardService.countArticle(search_option, keyword);
		Pager pager = new Pager(count, curPage);
		int start = pager.getPageBegin();
		int end = pager.getPageEnd();

		List<BoardDTO> list = boardService.listAll(start, end, search_option, keyword); // 목록
		ModelAndView mav = new ModelAndView();
		mav.setViewName("mypage/boardList");
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("list", list);
		map.put("count", count);
		map.put("search_option", search_option);
		map.put("keyword", keyword);
		map.put("pager", pager);
		mav.addObject("map", map);
		return mav;

	}

	// 공지사항 상세
	@RequestMapping("boardView")
	public ModelAndView view(@RequestParam int bNum, @RequestParam int curPage, @RequestParam String search_option, @RequestParam String keyword,
			HttpSession session) throws Exception {

		boardService.increaseViewcnt(bNum);
		ModelAndView mav = new ModelAndView();
		mav.setViewName("mypage/boardView");
		mav.addObject("dto", boardService.read(bNum));
		mav.addObject("curPage", curPage);
		mav.addObject("search_option", search_option);
		mav.addObject("keyword", keyword);

		return mav;
	}

}
