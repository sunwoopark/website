package com.website.mysite.controller.member;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.website.mysite.model.member.dto.MemberDTO;
import com.website.mysite.service.member.MemberService;

@Controller
@RequestMapping("/member/*")
public class MemberController {

	private static final Logger LOGGER = LoggerFactory.getLogger(MemberController.class);

	@Inject
	MemberService memberService;

	@Autowired
	BCryptPasswordEncoder passEncoder;

	// 회원 가입 동의 화면
	@RequestMapping("join")
	public String join() {
		return "member/join";
	}

	// 회원가입정보입력 화면
	@RequestMapping("joinAgree")
	public String joinAgree(ModelAndView mav, MemberDTO dto) {
		return "member/joinAgree";
	}

	// 회원 가입 결과
	@RequestMapping("joinAgreeResult")
	public ModelAndView joinAgreeResult(@ModelAttribute MemberDTO dto, HttpServletRequest request, ModelAndView mav) {

		String inputPass = dto.getUserPass();
		String pass = passEncoder.encode(inputPass);
		dto.setUserPass(pass);

		dto.setUserBirth(request.getParameter("birthYY") + request.getParameter("birthMM") + request.getParameter("birthDD"));
		dto.setUserPhone(request.getParameter("phoneFF") + request.getParameter("phoneSS") + request.getParameter("phoneTT"));
		memberService.joinAgree(dto);

		mav.setViewName("result");
		mav.addObject("message", "joinOk");
		return mav;
	}

	// 아이디 중복 체크
	@RequestMapping("idCheck")
	public int idCheck(MemberDTO dto) {
		int result = memberService.idCheck(dto);
		return result;
	}

	// 로그인 화면
	@RequestMapping("login")
	public String login() {
		return "member/login";
	}

	// 로그인 처리
	@RequestMapping("loginResult")
	public ModelAndView login(ModelAndView mav, MemberDTO dto, HttpSession session) {

		MemberDTO login = memberService.login(dto);

		boolean passMatch = passEncoder.matches(dto.getUserPass(), login.getUserPass());

		if (login != null && passMatch) {
			session.setAttribute("userId", login);
			mav.setViewName("result");
			mav.addObject("message", "loginOk");
		} else {
			mav.setViewName("result");
			mav.addObject("message", "error");
		}

		return mav;
	}

	// 로그아웃
	@RequestMapping("logout")
	public String logout(ModelAndView mav, HttpSession session) {
		session.invalidate();
		return "redirect:/";
	}

}
