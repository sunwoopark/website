package com.website.mysite.controller.shop;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.website.mysite.model.member.dto.MemberDTO;
import com.website.mysite.model.shop.dto.CartDTO;
import com.website.mysite.model.shop.dto.CartListDTO;
import com.website.mysite.service.shop.CartService;

@Controller
@RequestMapping("/cart/*")
public class CartController {

	private static final Logger LOGGER = LoggerFactory.getLogger(CartController.class);

	@Inject
	CartService cartService;

	@RequestMapping("insert")
	public String insert(@ModelAttribute CartDTO dto, HttpSession session) {
		MemberDTO userId = (MemberDTO) session.getAttribute("userId");
		dto.setUserId(userId.getUserId());
		cartService.insert(dto);

		return "redirect:/cart/list";
	}

	@RequestMapping("list")
	public ModelAndView list(HttpSession session, ModelAndView mav) {
		Map<String, Object> map = new HashMap<String, Object>();
		MemberDTO userId = (MemberDTO) session.getAttribute("userId");
		if (userId != null) {
			List<CartListDTO> list = cartService.listCart(userId.getUserId());
			int sumMoney = cartService.sumMoney(userId.getUserId());
			int fee = sumMoney >= 30000 ? 0 : 2500;
			map.put("sumMoney", sumMoney);
			map.put("fee", fee);
			map.put("sum", sumMoney + fee);
			map.put("list", list);
			map.put("count", list.size());
			mav.setViewName("shop/cartList");
			mav.addObject("map", map);
			return mav;
		} else {
			return new ModelAndView("member/login", "", null);
		}
	}

	@RequestMapping("delete")
	public String delete(@RequestParam int cartId) {
		cartService.delete(cartId);
		/* return "redirect:/shop/cartList"; */
		return "redirect:/cart/list";

	}

	@RequestMapping("deleteAll")
	public String deleteAll(HttpSession session) {
		MemberDTO userId = (MemberDTO) session.getAttribute("userId");
		cartService.deleteAll(userId.getUserId());
		return "redirect:/cart/list";
	}

	@RequestMapping("update")
	public String update(@RequestParam int amount, @RequestParam int cartId, CartDTO dto) {
		dto.setCartId(cartId);
		dto.setAmount(amount);

		cartService.modifyCart(dto);
		return "redirect:/cart/list";
	}

}
