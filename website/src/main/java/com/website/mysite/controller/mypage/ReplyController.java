package com.website.mysite.controller.mypage;

import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.website.mysite.model.board.dto.ReplyDTO;
import com.website.mysite.model.member.dto.MemberDTO;
import com.website.mysite.service.board.Pager;
import com.website.mysite.service.board.ReplyService;

@RestController
@RequestMapping("/reply/*")
public class ReplyController {

	@Inject
	ReplyService replyService;

	@RequestMapping("replyInsert")
	public void insert(ReplyDTO dto, HttpSession session) {
		MemberDTO userId = (MemberDTO) session.getAttribute("userId");
		dto.setReplyer(userId.getUserId());
		replyService.create(dto);
	}

	@RequestMapping("replyList")
	public ModelAndView list(int bNum, @RequestParam(defaultValue = "1") int curPage, ModelAndView mav, HttpSession session) {

		int count = replyService.count(bNum);
		Pager pager = new Pager(count, curPage);
		int start = pager.getPageBegin();
		int end = pager.getPageEnd();

		List<ReplyDTO> list = replyService.list(bNum, start, end, session);
		mav.setViewName("admin/board/replyList");
		mav.addObject("list", list);
		mav.addObject("pager", pager);

		return mav;
	}

}
