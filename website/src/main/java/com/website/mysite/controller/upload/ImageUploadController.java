package com.website.mysite.controller.upload;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class ImageUploadController {

	@ResponseBody
	@RequestMapping("imageUpload")
	public void imageUpload(HttpServletRequest request, HttpServletResponse response, @RequestParam MultipartFile upload) throws Exception {
		response.setCharacterEncoding("utf-8");
		response.setContentType("text/html; charset=utf-8");

		String fileName = upload.getOriginalFilename();
		byte[] bytes = upload.getBytes();

		String uploadPath = "D:\\website\\.metadata\\.plugins\\org.eclipse.wst.server.core\\tmp0\\wtpwebapps\\website\\resources\\images\\";
		OutputStream out = new FileOutputStream(new File(uploadPath + fileName));

		// 서버에저장
		out.write(bytes);
		String callback = request.getParameter("CKEditorFuncNum");
		PrintWriter printWriter = response.getWriter();
		String fileUrl = request.getContextPath() + "/images/" + fileName;
		System.out.println("fileUrl : " + fileUrl);
		printWriter.println("<script type='text/javascript'>" + "window.parent.CKEDITOR.tools.callFunction(" + callback + ",'" + fileUrl
				+ "','이미지를 업로드하였습니다.')" + "</script>");
//		printWriter.println("{\"filename\" : \"" + fileName + "\", \"uploaded\" : 1, \"url\":\"" + fileUrl + "\"}");

		// 스크림 닫기
		printWriter.flush();

	}

}
