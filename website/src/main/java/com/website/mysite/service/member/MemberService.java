package com.website.mysite.service.member;

import com.website.mysite.model.member.dto.MemberDTO;

public interface MemberService {

	// 회원가입
	public void joinAgree(MemberDTO dto);

	// 아이디 중복 체크
	public int idCheck(MemberDTO dto);

	// 로그인
	public MemberDTO login(MemberDTO dto);

}
