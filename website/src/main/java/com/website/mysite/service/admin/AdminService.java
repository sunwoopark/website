package com.website.mysite.service.admin;

import java.util.List;

import com.website.mysite.model.member.dto.MemberDTO;
import com.website.mysite.model.shop.dto.ProductDTO;

public interface AdminService {
	// 유저 목록
	public List<MemberDTO> userList();

	// 유저 정보 수정
	public MemberDTO userUpdate(int userNum);

	// 상품 등록
	public void productRegister(ProductDTO dto);

	// 상품 목록
	public List<ProductDTO> productList();

	// 상품 상세
	public ProductDTO productDetail(int productId);

	// 상품 수정
	public void productUpdate(ProductDTO dto);

	// 상품 삭제
	public void productDelete(int productId);

	// 상품 이미지 유알엘
	public String fileInfo(int productId);

}
