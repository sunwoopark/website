package com.website.mysite.service.admin;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.website.mysite.model.admin.dao.AdminDAO;
import com.website.mysite.model.member.dto.MemberDTO;
import com.website.mysite.model.shop.dto.ProductDTO;

@Service
public class AdminServiceImpl implements AdminService {

	@Inject
	AdminDAO adminDao;

	@Override
	public List<MemberDTO> userList() {
		return adminDao.userList();
	}

	@Override
	public MemberDTO userUpdate(int userNum) {
		return adminDao.userUpdate(userNum);
	}

	// 상품 등록
	@Override
	public void productRegister(ProductDTO dto) {
		adminDao.productRegister(dto);
	}

	// 상품 목록
	@Override
	public List<ProductDTO> productList() {
		return adminDao.productList();
	}

	@Override
	public ProductDTO productDetail(int productId) {
		return adminDao.productDetail(productId);
	}

	@Override
	public void productUpdate(ProductDTO dto) {
		adminDao.productUpdate(dto);
	}

	@Override
	public void productDelete(int productId) {
		adminDao.productDelete(productId);
	}

	@Override
	public String fileInfo(int productId) {
		return adminDao.fileInfo(productId);
	}

}
