package com.website.mysite.service.board;

import java.util.List;

import javax.servlet.http.HttpSession;

import com.website.mysite.model.board.dto.ReplyDTO;

public interface ReplyService {

	public List<ReplyDTO> list(Integer bNum, int start, int end, HttpSession session);

	public int count(int bNum);

	public void create(ReplyDTO dto); // 생성

	public void update(ReplyDTO dto);

	public void delete(Integer rNum);

	public ReplyDTO detail(int rNum);
}
