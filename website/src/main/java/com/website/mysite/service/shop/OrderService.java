package com.website.mysite.service.shop;

import java.util.List;

import com.website.mysite.model.shop.dto.CartListDTO;
import com.website.mysite.model.shop.dto.OrderDTO;

public interface OrderService {

	public void orderInsert(OrderDTO dto) throws Exception;

	public List<OrderDTO> orderList();

	public List<CartListDTO> orderList(String userId);

	public List<OrderDTO> orderView(String orderNum);

	public void delivery(OrderDTO dto);
}
