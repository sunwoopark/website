package com.website.mysite.service.shop;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.website.mysite.model.shop.dao.CartDAO;
import com.website.mysite.model.shop.dto.CartDTO;
import com.website.mysite.model.shop.dto.CartListDTO;

@Service
public class CartServiceImpl implements CartService {

	@Inject
	CartDAO cartDao;

	@Override
	public void insert(CartDTO dto) {
		cartDao.insert(dto);
	}

	@Override
	public List<CartListDTO> listCart(String userId) {
		return cartDao.listCart(userId);
	}

	@Override
	public int sumMoney(String userId) {
		return cartDao.sumMoney(userId);
	}

	@Override
	public List<CartDTO> cartMoney() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(int cartId) {
		cartDao.delete(cartId);
	}

	@Override
	public void deleteAll(String userId) {
		cartDao.deleteAll(userId);
	}

	@Override
	public void modifyCart(CartDTO dto) {
		cartDao.modifyCart(dto);
	}

	@Override
	public void update(int cartId) {
		// TODO Auto-generated method stub

	}

	@Override
	public int countCart(String userId, int productId) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void updateCart(CartDTO dto) {
		// TODO Auto-generated method stub

	}

}
