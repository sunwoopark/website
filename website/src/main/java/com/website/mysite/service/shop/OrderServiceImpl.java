package com.website.mysite.service.shop;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.website.mysite.model.shop.dao.OrderDAO;
import com.website.mysite.model.shop.dto.CartListDTO;
import com.website.mysite.model.shop.dto.OrderDTO;

@Service
public class OrderServiceImpl implements OrderService {

	@Inject
	OrderDAO orderDao;

	@Override
	public void orderInsert(OrderDTO dto) {
		orderDao.orderInsert(dto);
	}

	@Override
	public List<OrderDTO> orderList() {
		return orderDao.orderList();
	}

	@Override
	public List<CartListDTO> orderList(String userId) {
		return orderDao.orderList(userId);
	}

	@Override
	public List<OrderDTO> orderView(String orderNum) {
		return orderDao.orderView(orderNum);
	}

	// 배송상태
	@Override
	public void delivery(OrderDTO dto) {
		orderDao.delivery(dto);

	}

}
