package com.website.mysite.service.shop;

import java.util.List;
import java.util.Map;

import com.website.mysite.model.shop.dto.ProductDTO;

public interface ProductService {

	public List<ProductDTO> listProduct(int productCate1, int start, int end);

	public int countProduct(int productCate1);

	public List<ProductDTO> listSearch(int start, int end, String keyword);

	public int countSearch(String keyword);

	public ProductDTO detailProduct(int productId);

	// home
	public Map<String, List<ProductDTO>> listProduct(int productCate1);

	/*
	 * public void updateProduct(ProductDTO dto);
	 * 
	 * public void deleteProduct(int product_id);
	 * 
	 * public void insertProduct(ProductDTO dto);
	 * 
	 * public String fileInfo(int product_id);
	 */
}
