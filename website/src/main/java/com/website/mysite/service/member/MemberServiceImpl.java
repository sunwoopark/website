package com.website.mysite.service.member;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.website.mysite.model.member.dao.MemberDAO;
import com.website.mysite.model.member.dto.MemberDTO;

@Service
public class MemberServiceImpl implements MemberService {

	@Inject
	MemberDAO memberDao;

	@Override
	public void joinAgree(MemberDTO dto) {
		memberDao.joinAgree(dto);
	}

	@Override
	public int idCheck(MemberDTO dto) {
		return memberDao.idCheck(dto);
	}

	@Override
	public MemberDTO login(MemberDTO dto) {
		return memberDao.login(dto);
	}

}
