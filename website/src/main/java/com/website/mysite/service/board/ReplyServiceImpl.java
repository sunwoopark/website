package com.website.mysite.service.board;

import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Service;

import com.website.mysite.model.board.dao.ReplyDAO;
import com.website.mysite.model.board.dto.ReplyDTO;

@Service
public class ReplyServiceImpl implements ReplyService {

	@Inject
	ReplyDAO replyDao;

	@Override
	public void create(ReplyDTO dto) {
		replyDao.create(dto);
	}

	@Override
	public List<ReplyDTO> list(Integer bNum, int start, int end, HttpSession session) {
		return replyDao.list(bNum, start, end);
	}

	@Override
	public int count(int bNum) {
		return replyDao.count(bNum);
	}

	@Override
	public void update(ReplyDTO dto) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(Integer rNum) {
		// TODO Auto-generated method stub

	}

	@Override
	public ReplyDTO detail(int rNum) {
		// TODO Auto-generated method stub
		return null;
	}

}
