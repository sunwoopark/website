package com.website.mysite.service.shop;

import java.util.List;

import com.website.mysite.model.shop.dto.CartDTO;
import com.website.mysite.model.shop.dto.CartListDTO;

public interface CartService {

	public void insert(CartDTO dto);

	public List<CartListDTO> listCart(String userId);

	public int sumMoney(String userId);

	public void delete(int cartId);

	public void deleteAll(String userId);

	public void update(int cartId);

	public int countCart(String userId, int productId);

	public void updateCart(CartDTO dto);

	public void modifyCart(CartDTO dto);

	public List<CartDTO> cartMoney();

}
