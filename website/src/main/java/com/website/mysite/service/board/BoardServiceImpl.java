package com.website.mysite.service.board;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.website.mysite.model.board.dao.BoardDAO;
import com.website.mysite.model.board.dto.BoardDTO;

@Service
public class BoardServiceImpl implements BoardService {

	@Inject
	BoardDAO boardDao;

	@Override
	public List<BoardDTO> listAll(int start, int end, String search_option, String keyword) throws Exception {
		return boardDao.listAll(start, end, search_option, keyword);
	}

	@Override
	public void write(BoardDTO dto) throws Exception {
		boardDao.write(dto);
	}

	@Override
	public int countArticle(String search_option, String keyword) throws Exception {
		return boardDao.countArticle(search_option, keyword);
	}

	@Override
	public BoardDTO read(int bNum) throws Exception {
		return boardDao.read(bNum);
	}

	@Override
	public void increaseViewcnt(int bNum) throws Exception {
		boardDao.increaseViewcnt(bNum);
	}

	@Override
	public void deleteFile(String fullName) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<String> getAttach(int bno) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addAttach(String fullName) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateAttach(String fullName, int bno) {
		// TODO Auto-generated method stub

	}

	/**/
	@Override
	public void boardUpdate(BoardDTO dto) throws Exception {
		boardDao.boardUpdate(dto);
	}

	@Override
	public void boardDelete(int bNum) throws Exception {
		boardDao.boardDelete(bNum);

	}

	@Override
	public void boardReplyDelete(int bNum) throws Exception {
		boardDao.boardReplyDelete(bNum);
	}

}
