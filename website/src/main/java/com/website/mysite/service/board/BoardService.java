package com.website.mysite.service.board;

import java.util.List;

import com.website.mysite.model.board.dto.BoardDTO;

public interface BoardService {
	public void deleteFile(String fullName); // 첨부파일 삭제

	public List<String> getAttach(int bno); // 첨부파일 목록

	public void addAttach(String fullName); // 첨부파일 저장

	public void updateAttach(String fullName, int bno); // 첨부파일 수정

	public void write(BoardDTO dto) throws Exception; // 글쓰기

	public void boardUpdate(BoardDTO dto) throws Exception; // 글 수정

	public void boardDelete(int bNum) throws Exception; // 글 삭제

	public void boardReplyDelete(int bNum) throws Exception; // 리플 삭제

	public List<BoardDTO> listAll(int start, int end, String search_option, String keyword) throws Exception; // 글 목록

	public int countArticle(String search_option, String keyword) throws Exception; // 레코드 갯수 계산

	public BoardDTO read(int bNum) throws Exception; // 글읽기

	public void increaseViewcnt(int bNum) throws Exception; // 조회수 증가처리

}
