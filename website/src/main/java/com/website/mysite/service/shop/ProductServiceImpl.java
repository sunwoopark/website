package com.website.mysite.service.shop;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.website.mysite.model.shop.dao.ProductDAO;
import com.website.mysite.model.shop.dto.ProductDTO;

@Service
public class ProductServiceImpl implements ProductService {

	@Inject
	ProductDAO productDao;

	// home
	@Override
	public Map<String, List<ProductDTO>> listProduct(int productCate1) {

		Map<String, List<ProductDTO>> productMap = new HashMap<String, List<ProductDTO>>();
		List<ProductDTO> productList = productDao.productList(productCate1, 2);
		productMap.put("best", productList);

		productList = productDao.productList(productCate1, 3);
		productMap.put("category", productList);

		productList = productDao.productList(productCate1, 4);
		productMap.put("choice", productList);

		return productMap;
	}

	@Override
	public List<ProductDTO> listProduct(int productCate1, int start, int end) {
		return productDao.listProduct(productCate1, start, end);
	}

	@Override
	public int countProduct(int productCate1) {
		return productDao.countProduct(productCate1);
	}

	@Override
	public List<ProductDTO> listSearch(int start, int end, String keyword) {
		return productDao.listSearch(start, end, keyword);
	}

	@Override
	public int countSearch(String keyword) {
		return productDao.countSearch(keyword);
	}

	@Override
	public ProductDTO detailProduct(int productId) {
		return productDao.detailProduct(productId);
	}

	/*
	 * // home
	 * 
	 * @Override public Map<String, List<ProductDTO>> listProduct() {
	 * 
	 * Map<String, List<ProductDTO>> productMap = new HashMap<String, List<ProductDTO>>(); List<ProductDTO> productList = productDao.productList(2);
	 * 
	 * productMap.put("best", productList); productList = productDao.productList(3);
	 * 
	 * productMap.put("category", productList);
	 * 
	 * productList = productDao.productList(4); productMap.put("choice", productList);
	 * 
	 * return productMap; }
	 */

	/*
	 * @Override public void updateProduct(ProductDTO dto) { productDao.updateProduct(dto);
	 * 
	 * }
	 * 
	 * @Override public void deleteProduct(int product_id) { productDao.deleteProduct(product_id); }
	 * 
	 * @Override public void insertProduct(ProductDTO dto) { productDao.insertProduct(dto); }
	 * 
	 * @Override public String fileInfo(int product_id) { return productDao.fileInfo(product_id); }
	 */

}
